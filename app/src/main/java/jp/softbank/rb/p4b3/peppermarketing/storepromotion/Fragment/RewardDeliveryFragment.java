package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model.Reward;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.R;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.GiveReward;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.SendMessage;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PepperMemory;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.View.AutoResizeTextView;

public class RewardDeliveryFragment extends MarketingFragment {

    // リワード付与失敗時理由
    String failarReason = "";

    // 仮登録したかどうかフラグ
    boolean madeTemporalRegister = false;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_reward_delivery, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // 仮登録フラグ受け取り
        Bundle bundle = getArguments();
        if (bundle != null) {
            madeTemporalRegister = bundle.getBoolean("temp", false);
        }

        // 背景設定 + くじかどうかチェックをするかのチェック
        switch (PepperMemory.flow.getType()) {

            case 2201:
            case 2301:
            case 2001:
                ((AutoResizeTextView) view.findViewById(R.id.txt_send_item)).setText(getString(R.string.txt_send_reward));
                // SMS再送信であれば、送信処理に移行
                if (activity.timeOfSend > 0) {
                    sendMessage();
                    return;
                }
                checkReward();
                break;
            case 2203:
            case 2303:
            case 2003:
                ((AutoResizeTextView) view.findViewById(R.id.txt_send_item)).setText(getString(R.string.txt_send_message));
                // SMS再送信であれば、送信処理に移行
                if (activity.timeOfSend > 0) {
                    sendMessage();
                    return;
                }
                giveReward();
                break;
        }
    }

    // リワードチェック
    private void checkReward() {
        Reward reward = PepperMemory.selectedReward;
        if (reward.getType() == Reward.RewardType.LOTTERY) {
            // todo Pepperくじ？
        } else {
            // リワード付与
            giveReward();
        }
    }

    // リワード付与処理
    private void giveReward() {

        String accessToken = PepperMemory.accessToken;
        int memberId = PepperMemory.memberId;
        int rewardType = PepperMemory.selectedReward.getType();
        int rewardId = PepperMemory.selectedReward.getId();
        String flowCode = PepperMemory.flow.getCode();
        String pepperName = PepperMemory.pepperName;

        new GiveReward(accessToken, memberId, rewardType, rewardId, flowCode, pepperName, new GiveReward.GiveRewardCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, String message) {

                // 通信成功・失敗確認
                if (response.isSuccess) {
                    PepperMemory.smsText = "ペッパーからのお届けものです。\n" + message;
                    // ペッパー発話
                    sayPepper();

                } else {
                    // リワード付与失敗
                    failarReason = response.errorMessage;
                    // リワード付与失敗理由確認へ
                    checkFailarReason();
                }
            }
        }).excute();
    }

    // 行動履歴登録処理 ここではリワード付与失敗
//    private void registerHistory() {
//
//        String accessToken = PepperMemory.accessToken;
//        int memberId = PepperMemory.memberId;
//        String flowCode = PepperMemory.flow.getCode();
//        String pepperName = PepperMemory.pepperName;
//        int visitType = RegisterHistory.VisitType.REWARD_RECEIPT_FAILURE;
//
//        new RegisterHistory(accessToken, memberId, flowCode, pepperName, visitType, new RegisterHistory.RegisterHistoryCallback() {
//            @Override
//            public void onFinish(HttpTask.HttpResponse response, boolean isSuccess) {
//
//                // 通信成功・失敗確認
//                if (response.isSuccess) {
//                    if (isSuccess) {
//                        // リワード付与失敗理由確認へ
//                        checkFailarReason();
//                    }
//                } else {
//                    // APIエラー
//                    fragmentInterface.changeFragment(new ErrorFragment());
//                }
//            }
//        }).excute();
//    }

    // リワード付与失敗内容確認
    private void checkFailarReason() {

        Bundle bundle = new Bundle();

        final String EXPIRED = "expired";         // 期限切れ
        final String DEPLETED = "depleted";       // 売り切れ
        final String DISTRIBUTED = "distributed"; // 重複

        if (failarReason.contains(EXPIRED)) {
            // 期限切れ
            bundle.putString("failer", EXPIRED);
        } else if (failarReason.contains(DEPLETED)) {
            // 売り切れ処理
            bundle.putString("failer", DEPLETED);
        } else if (failarReason.contains(DISTRIBUTED)) {
            // 重複エラー
            bundle.putString("failer", DISTRIBUTED);
        }

        Fragment fragment = new RewardErrorFragment();
        fragment.setArguments(bundle);

        // エラー画面
        activity.changeFragment(fragment);
    }

    // ペッパー発話
    private void sayPepper() {

        String speechText = "";
        switch (PepperMemory.flow.getType()) {
            case 2201:
            case 2301:
            case 2001:
                speechText = getString(R.string.send_sms_promo_reward);
                break;
            case 2203:
            case 2303:
            case 2003:
                speechText = getString(R.string.send_sms_promo_message);
                break;
        }

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, speechText, true, new ActionCallback() {
            @Override
            public void onSuccess() {
                // sms送信
                sendMessage();
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }

    // SMS送信
    private void sendMessage() {

        String accessToken = PepperMemory.accessToken;
        int memberId = PepperMemory.memberId;
        String smsText = PepperMemory.smsText;

        new SendMessage(accessToken, memberId, smsText, new SendMessage.SendMessageCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, boolean isSuccess) {

                // 通信成功・失敗確認
                if (response.isSuccess) {

                    // 送信回数リセット
                    activity.timeOfSend = 0;
                    // 送信メッセージリセット
                    PepperMemory.smsText = "";

                    // 送信成功画面へ遷移
                    Fragment fragment = new SmsSendSuccessFragment();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("temp", madeTemporalRegister);
                    fragment.setArguments(bundle);
                    activity.changeFragment(fragment);
                } else {

                    // 送信回数加算
                    activity.timeOfSend++;
                    // 503エラー
                    if (response.status == 503) {
                        activity.timeOfSend = activity.limitOfSend;
                    }
                    // 送信失敗画面へ
                    activity.changeFragment(new SmsSendFailureFragment());
                }
            }
        }).execute();
    }
}
