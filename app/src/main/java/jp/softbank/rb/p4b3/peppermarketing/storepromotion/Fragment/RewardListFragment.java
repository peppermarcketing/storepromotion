package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model.Reward;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.R;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.View.PepperButton;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PepperMemory;

public class RewardListFragment extends MarketingFragment {

    PepperButton btnReward1;
    PepperButton btnReward2;
    PepperButton btnReward3;
    PepperButton btnReward4;
    List<PepperButton> buttons;

    TextView txtReward1;
    TextView txtReward2;
    TextView txtReward3;
    TextView txtReward4;
    List<TextView> texts;

    TextView txtMesaage;

    boolean isWaiting = true;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_reward_list, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI 設定
        btnReward1 = view.findViewById(R.id.btn_reward_1);
        btnReward2 = view.findViewById(R.id.btn_reward_2);
        btnReward3 = view.findViewById(R.id.btn_reward_3);
        btnReward4 = view.findViewById(R.id.btn_reward_4);
        buttons = Arrays.asList(btnReward1, btnReward2, btnReward3, btnReward4);

        txtReward1 = view.findViewById(R.id.txt_reward_1);
        txtReward2 = view.findViewById(R.id.txt_reward_2);
        txtReward3 = view.findViewById(R.id.txt_reward_3);
        txtReward4 = view.findViewById(R.id.txt_reward_4);
        texts = Arrays.asList(txtReward1, txtReward2, txtReward3, txtReward4);

        txtMesaage = view.findViewById(R.id.txt_reward_list_title);
        txtMesaage.setText(PepperMemory.flow.getRoboappMessage2());

        // 終了ボタン処理
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);
                buttonFinishListener.onClick(view);
            }
        });

        renderButtons();
    }

    @Override
    public void onStop() {
        super.onStop();
        isWaiting = false;
    }

    // OKボタン押下
    private void clickDone(final Reward reward) {

        Log.d(TAG, "reward id: " + reward.getId());

        // ボタンを使用不可にする
        allButtonSetActive(layoutRoot, false);

        activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, getString(R.string.reward_list_c), true, new ActionCallback() {
            @Override
            public void onSuccess() {

                PepperMemory.selectedReward = reward;

                switch (PepperMemory.flow.getType()) {
                    case 2201:
                        // QR読み取り画面へ
                        activity.changeFragment(new ReadQRFragment());
                        break;
                    case 2001:
                        // 電話番号入力画面
                        activity.changeFragment(new EnterPhoneNumberFragment());
                        break;
                }
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }

    // ボタン表示変更処理
    private void renderButtons() {

        final int BUTTON_NUM = 4; // ボタン表示の個数
        for (int i = 0; i < BUTTON_NUM; i++) {
            if (i >= PepperMemory.rewards.size()) {
                break;
            }
            final Reward reward = PepperMemory.rewards.get(i);
            texts.get(i).setText(reward.getTitle());
            // クリック時処理付加
            buttons.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickDone(reward);
                }
            });
        }
        for (int i = BUTTON_NUM; i > PepperMemory.rewards.size(); i--) {
            // リワードが設定されていないボタンを非表示にする-1
            buttons.get(i-1).setVisibility(View.INVISIBLE);
        }

        // 発話
        sayPepper();
    }

    // 発話
    private void sayPepper() {

        List<String> texts = Arrays.asList(
                getString(R.string.reward_list_a_1),
                getString(R.string.reward_list_a_2),
                getString(R.string.reward_list_a_3)
        );

        String speechText = texts.get((new Random()).nextInt(texts.size())) + PepperMemory.flow.getRoboappMessage1();

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, speechText, true, new ActionCallback() {
            @Override
            public void onSuccess() {

                // 待機状態
                isWaiting = true;
                onWaiting();
            }

            @Override
            public void onError(String errorMessage) {

            }

            @Override
            public void onCancel() {

            }
        });

        say.execute();
    }

    private void onWaiting() {

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!isWaiting) {
                    return;
                }

                List<String> texts = Arrays.asList(
                        getString(R.string.reward_list_wait_1),
                        getString(R.string.reward_list_wait_2),
                        getString(R.string.reward_list_wait_3),
                        getString(R.string.reward_list_wait_4)
                );

                // 発話中なら発話中断
                sayCancel();

                // 発話
                say = new Say(qiContext, texts.get(new Random().nextInt(texts.size())), true, new ActionCallback() {
                    @Override
                    public void onSuccess() {
                        onWaiting();
                    }

                    @Override
                    public void onError(String errorMessage) {
                        this.onSuccess();
                    }

                    @Override
                    public void onCancel() {
                        this.onSuccess();
                    }
                });

                say.execute();

            }
        }, 30000);
    }
}
