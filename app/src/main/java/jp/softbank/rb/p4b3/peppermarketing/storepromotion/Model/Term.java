package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model;

public class Term {
    private String termSbr = "";
    private String termUser = "";

    public void setTermSbr(String termSbr) {
        this.termSbr = termSbr;
    }

    public void setTermUser(String termUser) {
        this.termUser = termUser;
    }

    public String getTermSbr() {
        return termSbr;
    }

    public String getTermUser() {
        return termUser;
    }
}
