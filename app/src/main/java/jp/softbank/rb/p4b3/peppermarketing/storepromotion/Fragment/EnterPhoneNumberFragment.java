package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.R;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Advance.SayAnimate;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.PepperUtil;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.View.AutoResizeTextView;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.View.PepperButton;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.TemporalRegister;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PepperMemory;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Timeout;

public class EnterPhoneNumberFragment extends MarketingFragment {

    AutoResizeTextView txtPhoneNumber;

    PepperButton btn1;
    PepperButton btn2;
    PepperButton btn3;
    PepperButton btn4;
    PepperButton btn5;
    PepperButton btn6;
    PepperButton btn7;
    PepperButton btn8;
    PepperButton btn9;
    PepperButton btn0;
    PepperButton btnDelete;

    PepperButton btnBack;

    PepperButton btnDone;
    PepperButton btnContract;

    ImageView imgNumberMisstake;

    //入力されている電話番号
    String phoneNumber = "";

    TextView txtTitle;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_enter_phone_number_promo, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // button init
        btn0 = view.findViewById(R.id.button_phone_0);
        btn1 = view.findViewById(R.id.button_phone_1);
        btn2 = view.findViewById(R.id.button_phone_2);
        btn3 = view.findViewById(R.id.button_phone_3);
        btn4 = view.findViewById(R.id.button_phone_4);
        btn5 = view.findViewById(R.id.button_phone_5);
        btn6 = view.findViewById(R.id.button_phone_6);
        btn7 = view.findViewById(R.id.button_phone_7);
        btn8 = view.findViewById(R.id.button_phone_8);
        btn9 = view.findViewById(R.id.button_phone_9);
        btnDelete = view.findViewById(R.id.button_phone_del);
        btnDone = view.findViewById(R.id.marketing_button_done);
        btnContract = view.findViewById(R.id.kiyaku);
        btnBack = view.findViewById(R.id.btn_back);

        // 戻るボタン
        if (activity.timeOfSend != 0) {
            btnBack.setVisibility(View.GONE);
        } else {
            switch (PepperMemory.flow.getType()) {
                case 2001:
                case 2201:
                case 2203:
                case 2301:
                case 2303:
                    btnBack.setVisibility(View.VISIBLE);
                    break;
                case 2003:
                    btnBack.setVisibility(View.GONE);
                    break;
            }
        }

        // set cliclListeneer
        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("0");
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("1");
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("2");
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("3");
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("4");
            }
        });
        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("5");
            }
        });
        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("6");
            }
        });
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("7");
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("8");
            }
        });
        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("9");
            }
        });
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickDone();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enterPhoneNumber("");
            }
        });
        btnContract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickContract();
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickBack();
            }
        });

        // init active
        btnDelete.setActive(false);
        btnDone.setActive(false);

        // その他UI
        txtPhoneNumber = view.findViewById(R.id.txt_phone_number);
        txtPhoneNumber.setText("");
        imgNumberMisstake = view.findViewById(R.id.img_number_misstake);
        imgNumberMisstake.setVisibility(View.GONE);
        txtTitle = view.findViewById(R.id.lbl_please_enter);

        if (activity.timeOfSend > 0) {
            txtTitle.setText(R.string.please_enter_phone_number_2);
        } else {
            switch (PepperMemory.flow.getType()) {
                case 2003:
                    txtTitle.setText(PepperMemory.flow.getRoboappMessage2());
                    break;
                default:
                    txtTitle.setText(R.string.please_enter_phone_number_2);
            }
        }

        // 終了ボタン処理
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);
                buttonFinishListener.onClick(view);
            }
        });

        // タイムアウト設定
        if (activity.timeOfSend <= 0) {
            activity.timeout = new Timeout(180);
        } else {
            activity.timeout = new Timeout(60);
        }
        activity.timeout = new Timeout(180);
        activity.timeout.setTimeoutListener(new Timeout.TimeoutListener() {
            @Override
            public void onTimeout() {
                timeout();
            }
        });

        // 発話
        sayGreeting();
    }

    // 発話
    private void sayGreeting() {

        String speechText = "";
        List<String> texts;

        if (activity.timeOfSend > 0) {

            speechText = getString(R.string.please_enter_phone_number);

        } else {

            switch (PepperMemory.flow.getType()) {
                case 2001:
                case 2201:
                case 2203:
                case 2301:
                case 2303:
                    speechText = getString(R.string.please_enter_phone_number_next);
                    break;
                case 2003:
                    texts = Arrays.asList(
                            getString(R.string.please_enter_phone_number_message_a_1),
                            getString(R.string.please_enter_phone_number_message_a_2),
                            getString(R.string.please_enter_phone_number_message_a_3)
                    );
                    speechText = texts.get((new Random()).nextInt(texts.size())) + PepperMemory.flow.getRoboappMessage1();
                    break;

            }
        }

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, speechText, true, new ActionCallback() {
            @Override
            public void onSuccess() {
                // タイムアウトスタート
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        activity.timeout.start();
                    }
                });
            }

            @Override
            public void onError(String errorMessage) {
                onSuccess();
            }

            @Override
            public void onCancel() {
                onSuccess();
            }
        });

        say.execute();
    }

    // 戻るボタンクリック時
    private void clickBack() {

        // タイムアウトリセット
        activity.timeout.resetCount();

        activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, getString(R.string.back_understand), true, new ActionCallback() {
            @Override
            public void onSuccess() {

                // フロータイプで処理を分ける
                switch (PepperMemory.flow.getType()) {
                    case 2001:
                        if (PepperMemory.rewards.size() > 1) {
                            activity.changeFragment(new RewardListFragment());
                        } else {
                            activity.changeFragment(new RewardListOneFragment());
                        }
                        break;
                    case 2003:
                        break;
                    case 2201:
                    case 2203:
                    case 2301:
                    case 2303:
                        activity.changeFragment(new ReadQRFragment());
                        break;

                }
            }

            @Override
            public void onError(String errorMessage) {
                onSuccess();
            }

            @Override
            public void onCancel() {
                onSuccess();
            }
        });

        say.execute();
    }

    // 規約ボタンクリック時
    private void clickContract() {

        // タイムアウトリセット
        activity.timeout.resetCount();

        activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);

        // 発話前にボタンをクリック不可にする
        allButtonSetActive(layoutRoot, false);

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, getString(R.string.click_contract), true, new ActionCallback() {
            @Override
            public void onSuccess() {

                // 規約画面へ遷移
                activity.changeFragment(new SelectContractFragment());
            }

            @Override
            public void onError(String errorMessage) {
                onSuccess();
            }

            @Override
            public void onCancel() {
                onSuccess();
            }
        });

        say.execute();
    }

    // 次へボタンクリック時
    private void clickDone() {

        // タイムアウトリセット
        activity.timeout.resetCount();

        activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);

        // 桁数チェック
        if (phoneNumber.length() != 11) {

            // 発話中なら発話中断
            sayCancel();

            // 発話
            say = new Say(qiContext, getString(R.string.phone_number_not_11), true, new ActionCallback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(String errorMessage) {
                    onSuccess();
                }

                @Override
                public void onCancel() {
                    onSuccess();
                }
            });

            say.execute();

        } else {

            /*
            2201, 2203のときはウェブサイト検索
            2301, 2303のときは会員検索からのリワード配信
            2001, 2003のときも会員検索からのリワード配信
            */

            // タイムアウトストップ
            activity.timeout.stop();

            // 御礼の挨拶
            sayCancel();
            int motionId = PepperUtil.convertResourceID(qiContext.getApplicationContext(), "bow_a001");
            say = new SayAnimate(qiContext, getString(R.string.entered_number), motionId, new ActionCallback() {
                @Override
                public void onSuccess() {

                    // 再送の場合
                    if (activity.timeOfSend > 0) {

                        // フロー番号が2201もしくは2203（QRで会員Idを取得する）であるかつ、会員Idがー1（デフォルト）であるときは
                        // Web会員検索の流れになる。
                        if ((PepperMemory.flow.getType() == 2201 || PepperMemory.flow.getType() == 2203) && PepperMemory.memberId == -1) {
                            // Web会員検索
                            searchWebsite();
                        } else {
                            // SMS再送
                            // 仮登録処理
                            String accsessToken = PepperMemory.accessToken;
                            String flowCode = PepperMemory.flow.getCode();
                            int floeType = PepperMemory.flow.getType();
                            String pepperName = PepperMemory.pepperName;

                            new TemporalRegister(accsessToken, flowCode, floeType, pepperName, phoneNumber, new TemporalRegister.TemporalRegisterCallback() {
                                @Override
                                public void onFinish(HttpTask.HttpResponse response, int memberId, String message) {

                                    if (response.isSuccess) {
                                        PepperMemory.memberId = memberId;
                                        // SMS再送信
                                        Fragment fragment = new RewardDeliveryFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putBoolean("temp", true);
                                        fragment.setArguments(bundle);
                                        activity.changeFragment(fragment);
                                    } else {
                                        // 送信失敗画面へ
                                        activity.timeOfSend = activity.limitOfSend;
                                        activity.changeFragment(new SmsSendFailureFragment());
                                    }
                                }
                            }).excute();
                        }
                        return;
                    }

                    switch (PepperMemory.flow.getType()) {
                        case 2201:
                        case 2203:
                            searchWebsite();
                            break;
                        case 2301:
                        case 2303:
                        case 2001:
                        case 2003:
                            getMemberId();
                            break;
                    }

                }

                @Override
                public void onError(String errorMessage) {
                    onSuccess();
                }

                @Override
                public void onCancel() {
                    onSuccess();
                }
            });
            say.execute();
        }
    }

    // 携帯番号更新
    private void enterPhoneNumber(String number) {

        // タイムアウトリセット
        activity.timeout.resetCount();

        // 1文字削除
        if (number.equals("")) {

            activity.soundPool.play(activity.seCancel, 1.0f, 1.0f, 0, 0, 1.0f);

            // 消すので最低1文字は欲しい
            if (phoneNumber.length() < 1) {
                return;
            }

            // 一文字削除（文字列切り抜き）
            phoneNumber = phoneNumber.substring(0, phoneNumber.length() - 1);
        }

        else {

            // 携帯電話番号は11桁
            if (phoneNumber.length() >= 11) {
                return;
            }

            activity.soundPool.play(activity.seTap, 1.0f, 1.0f, 0, 0, 1.0f);

            phoneNumber += number;
        }

        // ****で表示する
        StringBuilder secret = new StringBuilder();
        for (int i = 0; i < phoneNumber.length(); i++) {
            secret.append("*");
        }

        // 文字数チェック
        // 11文字なら数字を非活性, 次へを活性化(発話中であった場合非活性)
        if (phoneNumber.length() >= 11) {
            changeButtonsState(false);
            btnDone.setActive(true);

        } else {
            changeButtonsState(true);
            btnDone.setActive(false);
        }
        // 1文字も無ければバックを非活性
        if (phoneNumber.length() <= 0) {
            btnDelete.setActive(false);
        } else {
            btnDelete.setActive(true);
        }

        // 電話番号入力チェック
        // 3文字になった時（だけでいいのか。。。）
        if (!number.equals("")) {
            if (phoneNumber.length() == 3) {
                // 先頭3文字をチェック
                String top = phoneNumber.substring(0, 3);
                if (top.equals("090") || top.equals("080") || top.equals("070")) {
                    Log.d(TAG, "phone number ok");
                } else {
                    showErrorPhoneNumber();
                }
            }
        }

        // *** を表示
        txtPhoneNumber.setText(secret.toString());
    }

    // 電話番号エラーを出す
    private void showErrorPhoneNumber() {
        imgNumberMisstake.setVisibility(View.VISIBLE);
        // 5秒で消す
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                imgNumberMisstake.setVisibility(View.GONE);
            }
        },5000);


        List<String> texts = Arrays.asList(
                getString(R.string.phone_number_misstale_1),
                getString(R.string.phone_number_misstale_2),
                getString(R.string.phone_number_misstale_3)
        );

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, texts.get((new Random()).nextInt(texts.size())), true, new ActionCallback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }

    // ボタン状態変化
    private void changeButtonsState(boolean isActive) {
        btn0.setActive(isActive);
        btn1.setActive(isActive);
        btn2.setActive(isActive);
        btn3.setActive(isActive);
        btn4.setActive(isActive);
        btn5.setActive(isActive);
        btn6.setActive(isActive);
        btn7.setActive(isActive);
        btn8.setActive(isActive);
        btn9.setActive(isActive);
    }

    // ウェブ検索画面へ遷移
    private void searchWebsite() {

        // タイムアウトストップ
        activity.timeout.stop();

        Fragment fragment = new SearchWebsiteFragment();
        Bundle bundle = new Bundle();
        bundle.putString("phone_number", phoneNumber);
        fragment.setArguments(bundle);
        activity.changeFragment(fragment);
    }

    // 会員番号を取得する
    private void getMemberId() {

        // タイムアウトストップ
        activity.timeout.stop();

        Fragment fragment = new GetMemberIdFragment();
        Bundle bundle = new Bundle();
        bundle.putString("phone_number", phoneNumber);
        fragment.setArguments(bundle);
        activity.changeFragment(fragment);
    }
}
