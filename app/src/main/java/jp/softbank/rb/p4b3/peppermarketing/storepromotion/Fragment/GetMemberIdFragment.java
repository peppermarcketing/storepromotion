package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.R;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.SearchMember;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.TemporalRegister;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.UpdateMembership;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PepperMemory;

// 2301, 2303, 2001, 2003 で使用
public class GetMemberIdFragment extends MarketingFragment {

    String phoneNumber;

    boolean madeTemporalRgister = false;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_get_member_id, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // 電話番号受け取り
        Bundle bundle = getArguments();
        if (bundle != null) {
            phoneNumber = bundle.getString("phone_number", "");
        }

        searchMember();
    }

    // 会員検索
    private void searchMember() {
        String accessToken = PepperMemory.accessToken;

        new SearchMember(accessToken, SearchMember.SearchType.TEL, phoneNumber, new SearchMember.SearchMemberCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, int memberId, String memberUrl) {

                // 会員だった
                if (response.isSuccess) {

                    PepperMemory.memberId = memberId;

                    // 会員情報更新
                    updateMembership();
                }
                // 会員でなかった
                else {

                    switch (response.status) {
                        case 404:
                        case 415:
                            Log.e(TAG, response.errorMessage);
                            // 仮登録
                            temporalRegister();
                            break;
                        default:
                            // APIエラー
                            activity.changeFragment(new ErrorFragment());
                    }
                }
            }
        }).execute();
    }

    // 会員情報更新
    private void updateMembership() {

        final String accessToken = PepperMemory.accessToken;
        int memberId = PepperMemory.memberId;
        int flowType = PepperMemory.flow.getType();
        String flowCode = PepperMemory.flow.getCode();
        String pepperName = PepperMemory.pepperName;

        new UpdateMembership(accessToken, memberId, flowType, flowCode, pepperName, new UpdateMembership.UpdateMembershipCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, int memberId, String message) {

                // 通信成功・失敗確認
                if (response.isSuccess) {
                    // 更新終了
                    rewardDelivery();
                } else {
                    // APIエラー
                    activity.changeFragment(new ErrorFragment());
                }
            }
        }).execute();
    }

    // 仮登録
    private void temporalRegister() {

        String accessToken = PepperMemory.accessToken;
        String flowCode = PepperMemory.flow.getCode();
        int flowType = PepperMemory.flow.getType();
        final String pepperName = PepperMemory.pepperName;

        new TemporalRegister(accessToken, flowCode, flowType, pepperName, phoneNumber, new TemporalRegister.TemporalRegisterCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, int memberId, String message) {

                // 通信成功・失敗確認
                if (response.isSuccess) {

                    PepperMemory.memberId = memberId;
                    madeTemporalRgister = true;

                    rewardDelivery();
                } else {
                    // APIエラー
                    activity.changeFragment(new ErrorFragment());
                }
            }
        }).excute();
    }

    // 行動履歴登録処理 今回は仮登録で
//    private void registerHistory() {
//
//        String accessToken = PepperMemory.accessToken;
//        int memberId = PepperMemory.memberId;
//        String flowCode = PepperMemory.flow.getCode();
//        String pepperName = PepperMemory.pepperName;
//        int visitType = RegisterHistory.VisitType.TEMPORARY_REGISTRATION;
//
//        new RegisterHistory(accessToken, memberId, flowCode, pepperName, visitType, new RegisterHistory.RegisterHistoryCallback() {
//            @Override
//            public void onFinish(HttpTask.HttpResponse response, boolean isSuccess) {
//
//                // 通信成功・失敗確認
//                if (response.isSuccess) {
//                    // 登録成功
//                    rewardDelivery();
//                } else {
//                    // APIエラー
//                    fragmentInterface.changeFragment(new ErrorFragment());
//                }
//            }
//        }).excute();
//    }

    // リワード配信処理へ
    private void rewardDelivery() {
        Fragment fragment = new RewardDeliveryFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("temp", madeTemporalRgister);
        fragment.setArguments(bundle);
        activity.changeFragment(fragment);
    }
}
