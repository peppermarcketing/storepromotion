package jp.softbank.rb.p4b3.peppermarketing.storepromotion.View;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.journeyapps.barcodescanner.ViewfinderView;

public class CustomViewfinderView extends ViewfinderView {
    public CustomViewfinderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override public void onDraw(Canvas canvas) { scannerAlpha = 0; super.onDraw(canvas); }
}
