package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.R;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.GeneralizeMember;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.PepperUtil;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PepperMemory;

public class SmsSendSuccessFragment extends MarketingFragment {

    // 仮登録したかどうかフラグ
    boolean madeTemporalRegister = false;

    TextView txtMessage;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        switch (PepperMemory.flow.getType()) {
            // クーポン配信
            case 2201:
            case 2301:
            case 2001:
                // 使用するレイアウト
                return inflater.inflate(R.layout.fragment_sms_send_success_reward, container, false);
            // メッセージ配信
            case 2203:
            case 2303:
            case 2003:
                // 使用するレイアウト
                return inflater.inflate(R.layout.fragment_sms_send_success_message, container, false);
            default:
                // 使用するレイアウト
                return inflater.inflate(R.layout.fragment_sms_send_success_reward, container, false);
        }
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // 仮登録フラグ受け取り
        Bundle bundle = getArguments();
        if (bundle != null) {
            madeTemporalRegister = bundle.getBoolean("temp", false);
        }

        txtMessage = view.findViewById(R.id.txt_sms_success);
        txtMessage.setText(PepperMemory.flow.getRoboappMessage4());


        switch (PepperMemory.flow.getType()) {
            case 2201:
            case 2203:
                sayPepper();
                break;
            case 2301:
            case 2303:
            case 2001:
            case 2003:
                // 仮登録をしたか確認
                if (madeTemporalRegister) {
                    // 本会員登録
                    generalizeMember();
                } else {
                    sayPepper();
                }
                break;
        }
    }

    // 発話
    private void sayPepper() {

        List<String> texts = Arrays.asList(
                getString(R.string.sms_send_success_promo_b_1),
                getString(R.string.sms_send_success_promo_b_2),
                getString(R.string.sms_send_success_promo_b_3)
        );

        String speechText = texts.get((new Random().nextInt(texts.size())));

//        switch (PepperMemory.flow.getType()) {
//            // クーポン配信
//            case 2201:
//            case 2301:
//            case 2001:
//                speechText = getString(R.string.sms_send_success_promo_coupou) + speechText;
//                break;
//            // メッセージ配信
//            case 2203:
//            case 2303:
//            case 2003:
//                speechText = getString(R.string.sms_send_success_promo_massage) + speechText;
//                break;
//        }

        speechText = PepperMemory.flow.getRoboappMessage3() + speechText;

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, speechText, true, new ActionCallback() {
            @Override
            public void onSuccess() {
                // アプリ終了
                PepperUtil.finishApplication(activity);
            }

            @Override
            public void onError(String errorMessage) {
                onSuccess();
            }

            @Override
            public void onCancel() {
                onSuccess();
            }
        });

        say.execute();
    }

//    // 行動履歴登録
//    private void registerHistory() {
//        int visitType = -1;
//        switch (PepperMemory.flow.getType()) {
//            case 2201:
//            case 2301:
//            case 2001:
//                visitType = RegisterHistory.VisitType.COUPON_RECEIPT;
//                break;
//            case 2203:
//            case 2303:
//            case 2003:
//                visitType = RegisterHistory.VisitType.MESSAGE_RECEIPT;
//                break;
//        }
//
//        String accessToken = PepperMemory.accessToken;
//        int memberId = PepperMemory.memberId;
//        String flowCode = PepperMemory.flow.getCode();
//        String pepperName = PepperMemory.pepperName;
//
//        new RegisterHistory(accessToken, memberId, flowCode, pepperName, visitType, new RegisterHistory.RegisterHistoryCallback() {
//            @Override
//            public void onFinish(HttpTask.HttpResponse response, boolean isSuccess) {
//
//                // 通信成功失敗確認
//                if (response.isSuccess) {
//
//                    switch (PepperMemory.flow.getType()) {
//                        case 2201:
//                        case 2203:
//                            sayPepper();
//                            break;
//                        case 2301:
//                        case 2303:
//                        case 2001:
//                        case 2003:
//                            // 仮登録をしたか確認
//                            if (madeTemporalRegister) {
//                                // 本会員登録
//                                generalizeMember();
//                            } else {
//                                sayPepper();
//                            }
//                            break;
//                    }
//                } else {
//                    // APIエラー
//                    fragmentInterface.changeFragment(new ErrorFragment());
//                }
//            }
//        }).excute();
//    }

    // 本会員移行
    private void generalizeMember() {

        String accessToken = PepperMemory.accessToken;
        int memberId = PepperMemory.memberId;

        new GeneralizeMember(accessToken, memberId, new GeneralizeMember.GeneralizeMemberCallBack() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, boolean isSuccess) {

                // 通信成功失敗確認
                if (response.isSuccess) {
                    // 発話
                    sayPepper();
                } else {
                    // APIエラー
                    activity.changeFragment(new ErrorFragment());
                }
            }
        }).excute();
    }

    // 行動履歴登録2
//    private void registerHistory2() {
//
//        String accessToken = PepperMemory.accessToken;
//        int memberId = PepperMemory.memberId;
//        String flowCode = PepperMemory.flow.getCode();
//        String pepperName = PepperMemory.pepperName;
//        int visitType = RegisterHistory.VisitType.GENERALIZE_MEMBER;
//
//        new RegisterHistory(accessToken, memberId, flowCode, pepperName, visitType, new RegisterHistory.RegisterHistoryCallback() {
//            @Override
//            public void onFinish(HttpTask.HttpResponse response, boolean isSuccess) {
//
//                // 通信成功失敗確認
//                if (response.isSuccess) {
//                    sayPepper();
//                } else {
//                    // APIエラー
//                    fragmentInterface.changeFragment(new ErrorFragment());
//                }
//            }
//        }).excute();
//    }
}
