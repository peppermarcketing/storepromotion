package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.R;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.View.PepperButton;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PepperMemory;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Timeout;

public class ReadQRFragment extends MarketingFragment {

    DecoratedBarcodeView barcodeView;

    PepperButton btnBack;
    PepperButton btnReadQrWebsite;

    TextView txtReadQr;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_read_qr, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI設定
        barcodeView = view.findViewById(R.id.barcode_view);

        btnReadQrWebsite = view.findViewById(R.id.btn_read_qr_website);
        btnReadQrWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // ウェブ検索画面へ
                searchWebsite();
            }
        });
        btnBack = view.findViewById(R.id.btn_back);

        txtReadQr = view.findViewById(R.id.txt_reaad_qr);
        switch (PepperMemory.flow.getType()) {
            case 2201:
                txtReadQr.setText(getString(R.string.txt_read_qr));
                break;
            case 2203:
            case 2301:
            case 2303:
                txtReadQr.setText(PepperMemory.flow.getRoboappMessage2());
                break;
        }

        // UI表示・非表示設定
        // 戻るボタン
        switch (PepperMemory.flow.getType()) {
            case 2201:
                btnBack.setVisibility(View.VISIBLE);
                break;
            case 2203:
            case 2301:
            case 2303:
                btnBack.setVisibility(View.GONE);
                break;
        }
        // もう一度送るボタン
        switch (PepperMemory.flow.getType()) {
            case 2201:
            case 2203:
                btnReadQrWebsite.setVisibility(View.VISIBLE);
                break;
            case 2301:
            case 2303:
                btnReadQrWebsite.setVisibility(View.GONE);
                break;
        }

        startPreview();

        // タイムアウト設定
        activity.timeout = new Timeout(300);
        activity.timeout.setTimeoutListener(new Timeout.TimeoutListener() {
            @Override
            public void onTimeout() {
                timeout();
            }
        });

        // 終了ボタン処理
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);
                buttonFinishListener.onClick(view);
            }
        });

        // 発話
        sayPepper();
        // QRキャプチャ開始
        startCapture();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopPreview();
    }

    // 発話
    private void sayPepper() {

        // 発話文言設定
        String speechText = "";
        List<String> texts;
        switch (PepperMemory.flow.getType()) {
            case 2301:
                texts = Arrays.asList(
                        getString(R.string.read_qr_coupon_1),
                        getString(R.string.read_qr_coupon_2),
                        getString(R.string.read_qr_coupon_3)
                );
                speechText += texts.get((new Random()).nextInt(texts.size()));
                break;
            case 2203:
            case 2303:
                texts = Arrays.asList(
                        getString(R.string.read_qr_massage_1),
                        getString(R.string.read_qr_massage_2),
                        getString(R.string.read_qr_massage_3)
                );
                speechText += texts.get((new Random()).nextInt(texts.size()));
                break;
        }

        switch (PepperMemory.flow.getType()) {
            case 2201:
                speechText += getString(R.string.read_qr_membership);
                break;
            case 2203:
            case 2301:
            case 2303:
                speechText += PepperMemory.flow.getRoboappMessage1();
                break;
        }

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, speechText, true, new ActionCallback() {
            @Override
            public void onSuccess() {
                // タイムアウトスタート
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        activity.timeout.start();
                    }
                });
            }

            @Override
            public void onError(String errorMessage) {

            }

            @Override
            public void onCancel() {

            }
        });

        say.execute();
    }

    // プレビュー開始
    private void startPreview() {
        barcodeView.resume();
    }

    // プレビュー終了
    private void stopPreview() {
        barcodeView.pause();
    }

    // キャプチャ開始
    private void startCapture() {
        barcodeView.decodeSingle(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                Log.d(TAG, result.getText());
                onReadQr(result.getText());
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints) {

            }
        });
    }

    // OR読み込み後
    private void onReadQr(final String qrText) {

        // タイムアウトリセット
        activity.timeout.resetCount();
        // 効果音
        activity.soundPool.play(activity.seBarcode, 1.0f, 1.0f, 0, 0, 1.0f);
        // 発話中ボタンを非アクティブに
        allButtonSetActive(layoutRoot, false);

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, getString(R.string.on_read_qr), true, new ActionCallback() {
            @Override
            public void onSuccess() {
                // 読み込み画面へ遷移
                Bundle bundle = new Bundle();
                bundle.putString("qrText", qrText);
                ConvertQRFragment fragment = new ConvertQRFragment();
                fragment.setArguments(bundle);
                activity.changeFragment(fragment);
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }

    // webサイト検索
    private void searchWebsite() {

        // タイムアウトリセット
        activity.timeout.resetCount();
        // 効果音
        activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);
        // 発話中ボタンを非アクティブに
        allButtonSetActive(layoutRoot, false);

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, getString(R.string.read_qr_website), true, new ActionCallback() {
            @Override
            public void onSuccess() {
                // 電話番号入力
                Fragment fragment = new EnterPhoneNumberFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("web", true);
                fragment.setArguments(bundle);
                activity.changeFragment(fragment);
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }
}
