package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model.Reward;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.R;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.ConvertQr;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.SearchMember;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.UpdateMembership;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PepperMemory;

public class ConvertQRFragment extends MarketingFragment {

    // リワード付与失敗時理由
    String failarReason = "";

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_convert_qr, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // データ受け取り
        String qrText = "";
        Bundle bundle = getArguments();
        if (bundle != null) {
            qrText = bundle.getString("qrText");
        }

        // バーコード変換処理
        convertQr(qrText);
    }

    // バーコード変換処理
    private void convertQr(String qrText) {

        String accessToken = PepperMemory.accessToken;

        new ConvertQr(accessToken, qrText, new ConvertQr.ConvertQrCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, int barcodeType, int memberId, int rewardType, int rewardId) {

                // 通信成功・失敗確認
                if (response.isSuccess) {

                    // フロータイプで処理を分ける
                    switch (PepperMemory.flow.getType()) {

                        case 2201: // Pepper会員証をつかう
                        case 2203:
                            // バーコードのタイプが会員証かチェック
                            if (barcodeType == ConvertQr.BarcodeType.MEMBERSHIP_CARD) {
                                // メモリに保存
                                PepperMemory.memberId = memberId;
                                // 会員検索へ
                                searchMember();

                            } else {
                                // 会員証ではなかった
                                activity.changeFragment(new ErrorFragment());
                                return;
                            }
                            break;

                        case 2301: // リワード配信QRをつかう
                        case 2303:
                            // バーコードのタイプがリワード配信かチェック
                            if (barcodeType == ConvertQr.BarcodeType.REWARD_DELVERY) {

                                // リワードをメモリに登録
                                Reward reward = new Reward();
                                reward.setId(rewardId);
                                reward.setType(rewardType);
                                PepperMemory.selectedReward = reward;

                                // 電話番号入力画面
                                onConvertQr(new EnterPhoneNumberFragment());

                            } else {
                                // リワード配信QRではなかった
                                activity.changeFragment(new ErrorFragment());
                                return;
                            }
                            break;

                        default:
                            break;
                    }
                } else {

                    switch (PepperMemory.flow.getType()) {
                        case 2201: // Pepper会員証だったばあいはエラーにしてよい
                        case 2203:
                            // バーコード変換エラー
                            activity.changeFragment(new ErrorFragment());
                            break;

                        case 2301: // リワードQR配信だった場合は理由をチェックしないといけない
                        case 2303:
                            failarReason = response.errorMessage;
                            checkFailarReason();
                            break;
                    }
                }
            }
        }).excute();
    }

    // 会員検索
    private void searchMember() {

        String accessToken = PepperMemory.accessToken;
        final int memberId = PepperMemory.memberId;

        new SearchMember(accessToken, SearchMember.SearchType.MEMBER_ID, String.valueOf(memberId), new SearchMember.SearchMemberCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, int memberId, String memberUrl) {

                // 通信成功・失敗確認
                if (response.isSuccess) {
                    // 通信成功 = 会員情報あり
                    // 会員情報更新
                    updateMembership();

                } else {
                    // 通信失敗 = 会員情報なし等
                    // 会員情報が無かったら404
                    if (response.status == 404) {
                        Log.d(TAG, "会員情報なし: " + memberId);
                    } else if (response.status == 415) {
                        Log.d(TAG, "仮登録、退会済、基本設定未了のいずれか");
                    }
                    // APIエラー
                    activity.changeFragment(new ErrorFragment());
                }
            }
        }).execute();
    }

    // 会員情報更新
    private void updateMembership() {

        final String accessToken = PepperMemory.accessToken;
        int memberId = PepperMemory.memberId;
        int flowType = PepperMemory.flow.getType();
        String flowCode = PepperMemory.flow.getCode();
        String pepperName = PepperMemory.pepperName;

        new UpdateMembership(accessToken, memberId, flowType, flowCode, pepperName, new UpdateMembership.UpdateMembershipCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, int memberId, String message) {

                // 通信成功・失敗確認
                if (response.isSuccess) {
                    // リワード配信処理へ移動
                    onConvertQr(new RewardDeliveryFragment());
                } else {
                    // APIエラー
                    activity.changeFragment(new ErrorFragment());
                }
            }
        }).execute();
    }

    // QR読み込み後
    private void onConvertQr(final Fragment fragment) {

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, getString(R.string.on_convert_qr), true, new ActionCallback() {
            @Override
            public void onSuccess() {
                activity.changeFragment(fragment);
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }

    // リワード付与失敗内容確認
    private void checkFailarReason() {

        Bundle bundle = new Bundle();

        final String EXPIRED = "expired";         // 期限切れ
        final String DEPLETED = "depleted";       // 売り切れ
        final String DISTRIBUTED = "distributed"; // 重複

        if (failarReason.contains(EXPIRED)) {
            // 期限切れ
            bundle.putString("failer", EXPIRED);
        } else if (failarReason.contains(DEPLETED)) {
            // 売り切れ処理
            bundle.putString("failer", DEPLETED);
        } else if (failarReason.contains(DISTRIBUTED)) {
            // 重複エラー
            bundle.putString("failer", DISTRIBUTED);
        }

        Fragment fragment = new RewardErrorFragment();
        fragment.setArguments(bundle);

        // エラー画面
        activity.changeFragment(fragment);
    }
}
