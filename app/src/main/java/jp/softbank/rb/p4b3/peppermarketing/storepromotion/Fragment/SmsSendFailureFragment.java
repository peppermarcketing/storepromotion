package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.R;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.View.PepperButton;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.PepperUtil;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Timeout;

public class SmsSendFailureFragment extends MarketingFragment {

    PepperButton btnOk;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_sms_send_failure_promo, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI設定
        btnOk = view.findViewById(R.id.btn_sms_failure_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickOk();
            }
        });

        if (activity.timeOfSend >= activity.limitOfSend) {
            // 4回目 or 503
            layoutRoot.setBackground(getResources().getDrawable(R.drawable.pepper_marketing_sms_failure_finish));
            btnOk.setVisibility(View.INVISIBLE);
            btnFinish.setVisibility(View.INVISIBLE);

            // 発話中なら発話中断
            sayCancel();

            // 発話
            say = new Say(qiContext, getString(R.string.sms_send_failure_finish), true, new ActionCallback() {
                @Override
                public void onSuccess() {
                    PepperUtil.finishApplication(activity);
                }

                @Override
                public void onError(String errorMessage) {
                    onSuccess();
                }

                @Override
                public void onCancel() {
                    onSuccess();
                }
            });

            say.execute();

        } else {
            // 電話番号再入力
            layoutRoot.setBackground(getResources().getDrawable(R.drawable.pepper_marketing_sms_failure));

            btnOk.setActive(false);
            btnFinish.setActive(false);

            // タイムアウト設定
            activity.timeout = new Timeout(60);
            activity.timeout.setTimeoutListener(new Timeout.TimeoutListener() {
                @Override
                public void onTimeout() {
                    timeout();
                }
            });

            sayPepper();
        }
    }

    // OKボタンの処理
    private void clickOk() {

        activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);

        allButtonSetActive(layoutRoot, false);

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, getString(R.string.sms_send_failure_ok), true, new ActionCallback() {
            @Override
            public void onSuccess() {
                activity.changeFragment(new EnterPhoneNumberFragment());
            }

            @Override
            public void onError(String errorMessage) {
                onSuccess();
            }

            @Override
            public void onCancel() {
                onSuccess();
            }
        });

        say.execute();
    }

    //
    private void sayPepper() {

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, getString(R.string.sms_send_failure), true, new ActionCallback() {
            @Override
            public void onSuccess() {

                // タイムアウトスタート
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        activity.timeout.start();
                    }
                });
            }

            @Override
            public void onError(String errorMessage) {
                onSuccess();
            }

            @Override
            public void onCancel() {
                onSuccess();
            }
        });

        say.execute();
    }
}
