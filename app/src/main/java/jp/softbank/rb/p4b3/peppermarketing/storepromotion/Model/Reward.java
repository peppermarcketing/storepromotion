package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model;

public class Reward {
    private int type;
    private int id;
    private String title;
    private boolean limitOver;
    private boolean termOver;

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setLimitOver(boolean limitOver) {
        this.limitOver = limitOver;
    }

    public void setTermOver(boolean termOver) {
        this.termOver = termOver;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getType() {
        return type;
    }

    public boolean isLimitOver() {
        return limitOver;
    }

    public boolean isTermOver() {
        return termOver;
    }

    static public class RewardType {
        public static final int COUPON = 1;
        public static final int LOTTERY = 2;
        public static final int MESSAGE = 3;
    }
}
