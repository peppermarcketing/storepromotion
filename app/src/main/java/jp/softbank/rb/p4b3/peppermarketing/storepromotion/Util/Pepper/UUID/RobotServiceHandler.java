package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.UUID;

import android.os.Handler;
import android.os.Message;

public class RobotServiceHandler extends Handler {
    private static final int ROBOT_UUID = 0;

    private final Callback callback;

    public RobotServiceHandler(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what) {
            case ROBOT_UUID:
                callback.onRobotUuidDRetrieved((String) msg.obj);
                break;
            default:
                super.handleMessage(msg);
        }

    }

    public void sendRobotUUID(String robotUUID) {
        Message msg = new Message();
        msg.what = ROBOT_UUID;
        msg.obj = robotUUID;
        sendMessage(msg);
    }

    public interface Callback {
        void onRobotUuidDRetrieved(String robotUUId);
    }
}
