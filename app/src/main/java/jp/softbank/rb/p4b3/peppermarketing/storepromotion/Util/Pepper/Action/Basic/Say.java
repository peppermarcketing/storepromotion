package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic;

import android.util.Log;

import com.aldebaran.qi.Consumer;
import com.aldebaran.qi.Future;
import com.aldebaran.qi.sdk.QiContext;
import com.aldebaran.qi.sdk.builder.SayBuilder;
import com.aldebaran.qi.sdk.object.conversation.BodyLanguageOption;
import com.aldebaran.qi.sdk.object.locale.Language;
import com.aldebaran.qi.sdk.object.locale.Locale;
import com.aldebaran.qi.sdk.object.locale.Region;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.PepperUtil;


public class Say {

    private final String TAG = "Say";

    private QiContext qiContext;
    private String text;
    private boolean withBodyLanguage;
    private ActionCallback callback;

    private boolean isWorking;

    private Future<Void> fSay;

    public Say(QiContext qiContext, String text, boolean withBodyLanguae, ActionCallback callback) {
        this.qiContext = qiContext;
        this.text = text;
        this.withBodyLanguage = withBodyLanguae;
        this.callback = callback;
    }

    public void execute() {

        isWorking = true;

        SayBuilder.with(qiContext)
                .withBodyLanguageOption(withBodyLanguage ? BodyLanguageOption.NEUTRAL: BodyLanguageOption.DISABLED)
                .withLocale(new Locale(Language.JAPANESE, Region.JAPAN))
                .withText(text)
                .buildAsync()
                .thenConsume(new Consumer<Future<com.aldebaran.qi.sdk.object.conversation.Say>>() {
                    @Override
                    public void consume(Future<com.aldebaran.qi.sdk.object.conversation.Say> sayFuture) throws Throwable {

                        fSay = sayFuture.get().async().run();

                        Log.d(TAG, "start :" + text);

                        fSay.thenConsume(new Consumer<Future<Void>>() {
                            @Override
                            public void consume(Future<Void> future) {

                                if (future.isDone()) {
                                    Log.d(TAG, "isDone");
                                    isWorking = false;

                                    if (callback == null) {
                                        Log.d(TAG, "callback is null");
                                        return;
                                    }

                                    if (future.isSuccess()) {
                                        Log.d(TAG, "isSuccess");
                                        callback.onSuccess();

                                    } else if (future.hasError()) {
                                        Log.d(TAG, "hasError");
                                        callback.onError(future.getErrorMessage());

                                    } else if (future.isCancelled()) {
                                        Log.d(TAG, "isCancelled");
                                        callback.onCancel();
                                    }

                                    // 直立に戻す
                                    new Animate(qiContext, PepperUtil.convertResourceID(qiContext.getApplicationContext(), "stand_simple"), null).execute();
                                }
                            }
                        });
                    }
                });
    }

    public void cancel() {
        if (fSay != null) {
            fSay.cancel(true);
        }
    }

    public boolean isWorking() {
        return isWorking;
    }
}
