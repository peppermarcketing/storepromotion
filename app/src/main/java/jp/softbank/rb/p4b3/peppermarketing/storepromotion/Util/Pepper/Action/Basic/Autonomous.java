package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic;

import android.util.Log;

import com.aldebaran.qi.Consumer;
import com.aldebaran.qi.Future;
import com.aldebaran.qi.sdk.QiContext;
import com.aldebaran.qi.sdk.builder.HolderBuilder;
import com.aldebaran.qi.sdk.object.holder.AutonomousAbilitiesType;
import com.aldebaran.qi.sdk.object.holder.Holder;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;

public class Autonomous {

    private final String TAG = "Autonomous";

    private QiContext qiContext;
    private ActionCallback callback;

    private Holder holder;

    public Autonomous(QiContext qiContext, ActionCallback callback) {
        this.qiContext = qiContext;
        this.callback = callback;
    }

    public void hold() {

        // Build the holder for the abilities.
        holder = HolderBuilder.with(qiContext)
                .withAutonomousAbilities(
                        AutonomousAbilitiesType.BACKGROUND_MOVEMENT,
                        AutonomousAbilitiesType.BASIC_AWARENESS,
                        AutonomousAbilitiesType.AUTONOMOUS_BLINKING
                )
                .build();

        // Hold the abilities asynchronously.
        Future<Void> fHold = holder.async().hold();

        fHold.thenConsume(new Consumer<Future<Void>>() {
            @Override
            public void consume(Future<Void> future) {

                if (future.isDone()) {
                    Log.d(TAG, "isDone");

                    if (callback == null) {
                        Log.d(TAG, "callback is null");
                        return;
                    }

                    if (future.isSuccess()) {
                        Log.d(TAG, "isSuccess");
                        callback.onSuccess();

                    } else if (future.hasError()) {
                        Log.d(TAG, "hasError");
                        callback.onError(future.getErrorMessage());

                    } else if (future.isCancelled()) {
                        Log.d(TAG, "isCancelled");
                        callback.onCancel();
                    }
                }
            }
        });
    }

    public void release() {

        // Release the holder asynchronously.
        Future<Void> fRelease = holder.async().release();

        fRelease.thenConsume(new Consumer<Future<Void>>() {
            @Override
            public void consume(Future<Void> future) {

                if (future.isDone()) {
                    Log.d(TAG, "isDone");

                    if (callback == null) {
                        Log.d(TAG, "callback is null");
                        return;
                    }

                    if (future.isSuccess()) {
                        Log.d(TAG, "isSuccess");
                        callback.onSuccess();

                    } else if (future.hasError()) {
                        Log.d(TAG, "hasError");
                        callback.onError(future.getErrorMessage());

                    } else if (future.isCancelled()) {
                        Log.d(TAG, "isCancelled");
                        callback.onCancel();
                    }
                }
            }
        });
    }
}
