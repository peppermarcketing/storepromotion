package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.R;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.SearchMember;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.SendMessage;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PepperMemory;

public class SearchWebsiteFragment extends MarketingFragment {

    View layoutBack;

    String phoneNumber;

    int memberId;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_search_website, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI設定
        layoutBack = view.findViewById(R.id.layout_root);

        // 電話番号受け取り
        Bundle bundle = getArguments();
        if (bundle != null) {
            phoneNumber = bundle.getString("phone_number", "");
        }

        // 発話
        sayPepper();

    }

    // 発話
    private void sayPepper() {

        List<String> texts = Arrays.asList(
                getString(R.string.searching_website_1),
                getString(R.string.searching_website_2),
                getString(R.string.searching_website_3)
        );

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, texts.get((new Random().nextInt(texts.size()))), true, new ActionCallback() {
            @Override
            public void onSuccess() {
                // 会員検索
                serchMember();
            }

            @Override
            public void onError(String errorMessage) {

            }

            @Override
            public void onCancel() {

            }
        });

        say.execute();
    }

    // 会員検索
    private void serchMember() {

        String accessToken = PepperMemory.accessToken;

        new SearchMember(accessToken, SearchMember.SearchType.TEL, phoneNumber, new SearchMember.SearchMemberCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, int memberId_, String memberUrl) {

                // 会員だった
                if (response.isSuccess) {

                    memberId = memberId_;
                    PepperMemory.smsText = "このURLからQRコードを表示できます。\n" + memberUrl;

                    // sms送信
                    sendSms();
                }
                // 会員でなかった
                else {
                    // TODO 会員で無いエラー

                    // APIエラー
                    activity.changeFragment(new ErrorFragment());

                }
            }
        }).execute();
    }

    // SMS送信
    private void sendSms() {

        String accessToken = PepperMemory.accessToken;
        String smsText = PepperMemory.smsText;

        new SendMessage(accessToken, memberId, smsText, new SendMessage.SendMessageCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, boolean isSuccess) {

                // 通信成功失敗判定
                if (response.isSuccess) {

                    // 送信回数リセット
                    activity.timeOfSend = 0;
                    // 送信メッセージリセット
                    PepperMemory.smsText = "";

                    // sms送信成功
                    sendSuccess();
                } else {

                    // 送信回数加算
                    activity.timeOfSend++;
                    // 503エラー
                    if (response.status == 503) {
                        activity.timeOfSend = activity.limitOfSend;
                    }
                    // 送信失敗画面へ
                    activity.changeFragment(new SmsSendFailureFragment());
                }

            }
        }).execute();
    }

    // sms送信成功
    private void sendSuccess() {

        // 画像変える
        layoutBack.setBackground(activity.getDrawable(R.drawable.pepper_marketing_send_website));

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, getString(R.string.send_website), true, new ActionCallback() {
            @Override
            public void onSuccess() {

                // sms送信回数リセット
                activity.timeOfSend = 0;

                // 画面戻る
                activity.changeFragment(new ReadQRFragment());
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }
}
