package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper;

import android.app.Activity;
import android.content.Context;
import android.os.Process;
import android.util.Log;

import com.aldebaran.qi.Consumer;
import com.aldebaran.qi.Future;
import com.aldebaran.qi.sdk.QiContext;
import com.aldebaran.qi.sdk.builder.AnimateBuilder;
import com.aldebaran.qi.sdk.builder.AnimationBuilder;
import com.aldebaran.qi.sdk.builder.ChatBuilder;
import com.aldebaran.qi.sdk.builder.HolderBuilder;
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder;
import com.aldebaran.qi.sdk.builder.SayBuilder;
import com.aldebaran.qi.sdk.builder.TopicBuilder;
import com.aldebaran.qi.sdk.object.actuation.Animation;
import com.aldebaran.qi.sdk.object.conversation.BodyLanguageOption;
import com.aldebaran.qi.sdk.object.conversation.Chat;
import com.aldebaran.qi.sdk.object.conversation.QiChatbot;
import com.aldebaran.qi.sdk.object.conversation.Say;
import com.aldebaran.qi.sdk.object.conversation.Topic;
import com.aldebaran.qi.sdk.object.holder.AutonomousAbilitiesType;
import com.aldebaran.qi.sdk.object.holder.Holder;
import com.aldebaran.qi.sdk.object.human.Human;
import com.aldebaran.qi.sdk.object.humanawareness.HumanAwareness;
import com.aldebaran.qi.sdk.object.locale.Language;
import com.aldebaran.qi.sdk.object.locale.Locale;
import com.aldebaran.qi.sdk.object.locale.Region;

import java.util.List;

public class PepperUtil {

    static String TAG = PepperUtil.class.getSimpleName();
    static public Future<Void> currentAnimate;
    static public Future<Void> currentChat;
    static public Future<Void> currentSay;

    // for autonomous
    static Holder currentHolder;

    //for say with animate
    static boolean isSpeaking;
    static boolean isAnimating;

    //リソースIDに変換する
    public static int convertResourceID(Context context, String filename){
        int strId = context.getResources().getIdentifier(filename, "raw", context.getPackageName());
        return strId;
    }

    // アプリ終了
    public static void finishApplication(Activity activity) {
        activity.finish();
        Process.killProcess(Process.myPid());
    }

    // say
    public static void pepperSay(QiContext qiContext, final String word, final FutureInterface futureInterface) {

        // qiContext チェック
        if (qiContext == null) {
            return;
        }

        Locale sampleLocale = new Locale(Language.JAPANESE, Region.JAPAN);

        // 非同期のSay build
        Future<Say> futureSayBuild = SayBuilder.with(qiContext)
                .withLocale(sampleLocale)
                .withText(word)
                .withBodyLanguageOption(BodyLanguageOption.DISABLED)
                .buildAsync();

        // say build完了
        futureSayBuild.thenConsume(new Consumer<Future<Say>>() {
            @Override
            public void consume(Future<Say> sayFuture) throws Throwable {

                // say
                currentSay = sayFuture.get().async().run();

                Log.d("say", "start say: " + word);

                // Say終了後の処理？
                currentSay.thenConsume(new Consumer<Future<Void>>() {
                    @Override
                    public void consume(Future<Void> future) throws Throwable {

                        if (future.isDone()) {
                            Log.d(TAG, "say is done");
                        }

                        if (future.isSuccess()) {
                            Log.d(TAG, "say is success");

                            if (futureInterface != null) {
                                futureInterface.onSuccess();
                            }
                        }

                        if (future.hasError()) {
                            Log.d(TAG, "say has error " + future.getErrorMessage());

                            if (futureInterface != null) {
                                futureInterface.onError(future.getErrorMessage());
                            }
                        }

                        if (future.isCancelled()) {
                            Log.d(TAG, "say is cancelled");
                        }
                    }
                });
            }
        });
    }

    // ランダムモーション付きSay
    public static void pepperSayWithRundomMotion(final QiContext qiContext, final String word, final FutureInterface futureInterface) {

        // qiContext チェック
        if (qiContext == null) {
            return;
        }

        Locale sampleLocale = new Locale(Language.JAPANESE, Region.JAPAN);

        // 非同期のSay build
        Future<Say> futureSayBuild = SayBuilder.with(qiContext)
                .withLocale(sampleLocale)
                .withText(word)
                .withBodyLanguageOption(BodyLanguageOption.NEUTRAL)
                .buildAsync();

        // say build完了
        futureSayBuild.thenConsume(new Consumer<Future<Say>>() {
            @Override
            public void consume(Future<Say> sayFuture) throws Throwable {

                // say
                currentSay = sayFuture.get().async().run();

                Log.d("say", "start say: " + word);

                // Say終了後の処理？
                currentSay.thenConsume(new Consumer<Future<Void>>() {
                    @Override
                    public void consume(Future<Void> future) throws Throwable {

                        if (future.isDone()) {
                            Log.d(TAG, "say is done");

                            // 直立に戻す
                            pepperAnimate(qiContext, convertResourceID(qiContext.getApplicationContext(), "stand_simple"), null);
                        }

                        if (future.isSuccess()) {
                            Log.d(TAG, "say is success");

                            if (futureInterface != null) {
                                futureInterface.onSuccess();
                            }
                        }

                        if (future.hasError()) {
                            Log.d(TAG, "say has error " + future.getErrorMessage());

                            if (futureInterface != null) {
                                futureInterface.onError(future.getErrorMessage());
                            }
                        }

                        if (future.isCancelled()) {
                            Log.d(TAG, "say is cancelled");
                        }
                    }
                });
            }
        });
    }

    // chat
    public static void pepperChat(QiContext qiContext, int topicId,
                                  Chat.OnListeningChangedListener listeningChangedListener,
                                  Chat.OnHeardListener heardListener,
                                  final QiChatbot.OnEndedListener endedListener,
                                  final FutureInterface futureInterface) {

        if (qiContext == null) {
            return;
        }

        // Create a topic
        Topic topic = TopicBuilder.with(qiContext)
                .withResource(topicId)
                .build();

        // Create a QiChatbot
        final QiChatbot qichatbot = QiChatbotBuilder.with(qiContext)
                .withTopic(topic)
                .build();
        qichatbot.setSpeakingBodyLanguage(BodyLanguageOption.DISABLED);

        // Create a Chat
        final Chat chat = ChatBuilder.with(qiContext)
                .withChatbot(qichatbot)
                .build();

        chat.addOnListeningChangedListener(listeningChangedListener);
        chat.addOnHeardListener(heardListener);

        // Execute the chat asynchronously
        currentChat = chat.async().run();

        currentChat.thenConsume(new Consumer<Future<Void>>() {
            @Override
            public void consume(Future<Void> future) throws Throwable {

                if (future.isSuccess()) {
                    Log.d(TAG, "chat is success");

                    if (futureInterface != null) {
                        futureInterface.onSuccess();
                    }
                }

                if (future.hasError()) {
                    Log.d(TAG, "chat has error: " + future.getErrorMessage());
                }

                if (future.isCancelled()) {
                    Log.d(TAG, "chat is cancelled");
                }
            }
        });

        // Stop the chat when the qichatbot is done
        qichatbot.addOnEndedListener(new QiChatbot.OnEndedListener() {
            @Override
            public void onEnded(String endReason) {
                currentChat.requestCancellation();
                if (endedListener == null) {
                    return;
                }
                endedListener.onEnded(endReason);
            }
        });
    }

    // animate
    public static void pepperAnimate(final QiContext qiContext, int animationId, final FutureInterface futureInterface) {

        if (qiContext == null) {
            return;
        }

        // Create an animation object.
        Future<Animation> animationFuture = AnimationBuilder.with(qiContext)
                .withResources(animationId)
                .buildAsync();

        animationFuture.thenConsume(new Consumer<Future<Animation>>() {
            @Override
            public void consume(Future<Animation> animationFuture_) throws Throwable {

                currentAnimate = AnimateBuilder.with(qiContext)
                        .withAnimation(animationFuture_.get())
                        .build()
                        .async()
                        .run();

                currentAnimate.thenConsume(new Consumer<Future<Void>>() {
                    @Override
                    public void consume(Future<Void> future) throws Throwable {

                        if (future.isDone()) {
                            Log.d(TAG, "animate is done");
                        }

                        if (future.isSuccess()) {
                            Log.d(TAG, "animate is success");

                            if (futureInterface != null) {
                                futureInterface.onSuccess();
                            }
                        }

                        if (future.hasError()) {
                            Log.d(TAG, "animate has error " + future.getErrorMessage());

                            if (futureInterface != null) {
                                futureInterface.onError(future.getErrorMessage());
                            }
                        }

                        if (future.isCancelled()) {
                            Log.d(TAG, "animate is cancelled");
                        }
                    }
                });
            }
        });
    }

    // say and animate
    public static void pepperSayWithMotion(QiContext qiContext, String word, int animateId, final FutureInterface futureInterface) {

        if (qiContext == null) {
            return;
        }

        isSpeaking = true;
        isAnimating = true;

        FutureInterface futureInterfaceSay = new FutureInterface() {
            @Override
            public void onSuccess() {
                isSpeaking = false;
                if (!isSpeaking && !isAnimating){

                    if (futureInterface != null) {
                        futureInterface.onSuccess();
                    }
                }
            }

            @Override
            public void onError(String error) {

            }
        };

        FutureInterface futureInterfaceAnimate = new FutureInterface() {
            @Override
            public void onSuccess() {
                isAnimating = false;
                if (!isSpeaking && !isAnimating){

                    if (futureInterface != null) {
                        futureInterface.onSuccess();
                    }
                }
            }

            @Override
            public void onError(String error) {

            }
        };

        pepperAnimate(qiContext, animateId, futureInterfaceAnimate);
        pepperSay(qiContext, word, futureInterfaceSay);
    }

    // autonoumas hold
    public static void pepperHoldAbilities(QiContext qiContext, final FutureInterface futureInterface) {

        if (qiContext == null) {
            return;
        }

        // Build the holder for the abilities.
        currentHolder = HolderBuilder.with(qiContext)
                .withAutonomousAbilities(
                        AutonomousAbilitiesType.BACKGROUND_MOVEMENT,
                        AutonomousAbilitiesType.BASIC_AWARENESS,
                        AutonomousAbilitiesType.AUTONOMOUS_BLINKING
                )
                .build();

        // Hold the abilities asynchronously.
        Future<Void> holdFuture = currentHolder.async().hold();

        // run
        holdFuture.thenConsume(new Consumer<Future<Void>>() {
            @Override
            public void consume(Future<Void> future) throws Throwable {

                if (future.isDone()) {
                    Log.d(TAG, "hold is done");
                }

                if (future.isSuccess()) {
                    Log.d(TAG, "hold is success");

                    if (futureInterface != null) {
                        futureInterface.onSuccess();
                    }
                }

                if (future.hasError()) {
                    Log.d(TAG, "hold has error " + future.getErrorMessage());

                    if (futureInterface != null) {
                        futureInterface.onError(future.getErrorMessage());
                    }
                }

                if (future.isCancelled()) {
                    Log.d(TAG, "hold is cancelled");
                }
            }
        });
    }

    // release autonoumas
    public static void releaseAbilities(final FutureInterface futureInterface) {

        // Release the holder asynchronously.
        Future<Void> releaseFuture = currentHolder.async().release();

        // run
        releaseFuture.thenConsume(new Consumer<Future<Void>>() {
            @Override
            public void consume(Future<Void> future) throws Throwable {

                if (future.isDone()) {
                    Log.d(TAG, "release is done");
                }

                if (future.isSuccess()) {
                    Log.d(TAG, "release is success");

                    if (futureInterface != null) {
                        futureInterface.onSuccess();
                    }
                }

                if (future.hasError()) {
                    Log.d(TAG, "release has error " + future.getErrorMessage());

                    if (futureInterface != null) {
                        futureInterface.onError(future.getErrorMessage());
                    }
                }

                if (future.isCancelled()) {
                    Log.d(TAG, "release is cancelled");
                }
            }
        });
    }

    public static void pepperEmotion(final QiContext qiContext){
        HumanAwareness humanAwareness = qiContext.getHumanAwareness();
        Future<List<Human>> humansAroundFuture;
        humansAroundFuture = humanAwareness.async().getHumansAround();
        final PeopleCharacteristics peopleCharacteristics = new PeopleCharacteristics();
        //peopleCharacteristics.retrieveCharacteristics(humansAroundFuture)
        humansAroundFuture.andThenConsume(new Consumer<List<Human>>() {
            @Override
            public void consume(List<Human> humansAround) throws Throwable {
                if (!humansAround.isEmpty()) {
                    Log.d(TAG, "Humans are :"+String.valueOf(humansAround.size()));
                    peopleCharacteristics.retrieveCharacteristics(qiContext,humansAround);
                }else{
                    Log.d(TAG, "can't catch the face!");
                }
            }
        });
    }
}
