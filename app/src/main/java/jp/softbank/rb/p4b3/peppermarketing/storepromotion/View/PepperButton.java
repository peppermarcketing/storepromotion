package jp.softbank.rb.p4b3.peppermarketing.storepromotion.View;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

@SuppressLint("AppCompatCustomView")
public class PepperButton extends ImageButton {

    private boolean isActive = true;

    public boolean currentActiveState = true;

    public PepperButton(Context context) {
        super(context, null);
    }

    public PepperButton(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public PepperButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        isActive = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                currentActiveState = isActive;
                setActive(false);
                return true;

            case MotionEvent.ACTION_UP:
                setActive(currentActiveState);
                return true;
        }
        return false;
    }

   @Override
   public void setOnClickListener(final OnClickListener listener) {

        super.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isActive) {
                    listener.onClick(view);
                }
            }
        });
   }

    // 非活性設定
    public void setActive(boolean active) {
        isActive = active;
        //this.setEnabled(isActive);
        if (isActive) {
            this.setAlpha(1.0f);
        } else {
            this.setAlpha(0.5f);
        }
    }
}
