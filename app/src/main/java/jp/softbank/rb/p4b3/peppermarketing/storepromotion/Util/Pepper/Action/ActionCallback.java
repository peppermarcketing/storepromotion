package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action;

public interface ActionCallback {
    void onSuccess();
    void onError(String errorMessage);
    void onCancel();
}
