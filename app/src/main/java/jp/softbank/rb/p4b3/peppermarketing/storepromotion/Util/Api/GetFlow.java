package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model.Flow;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model.Reward;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model.Term;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.JsonUtil;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Logutil;


public class GetFlow {

    private final String TAG = this.getClass().getSimpleName();

    private String accessToken;
    private String appId;
    private GetFlowCallback callback;

    public GetFlow(String accessToken, String appId, GetFlowCallback callback) {
        this.accessToken = accessToken;
        this.appId = appId;
        this.callback = callback;
    }

    // フロー情報取得
    public void excute() {

        Logutil.log("API_開始_フロー取得処理");

        final String url = "https://rap-vm.softbankrobotics.com/portal/pmkt/api3.php/pepper/flow";

        // POSTのヘッダー情報付加
        ArrayList<HttpTask.Param> headers = new ArrayList<>();
        headers.add(new HttpTask.Param("Content-Type", "application/x-www-form-urlencoded"));

        // POSTパラメータ
        String param = "access_token=" + accessToken + "&" +
                "app_id=" + appId + "&" +
                "android=1";

        // 引数(URL,パラメータ,ヘッダー)
        final HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                param,
                headers
        );

        // 通信前処理、通信後処理を設定
        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    Logutil.log("API_終了_フロー取得処理");

                    // 通信成功
                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);
                    // パース
                    Flow flow = getFlow(resultJson);
                    ArrayList<Reward> rewards = getRewardList(resultJson);
                    Term term = getTerm(resultJson);
                    callback.onFinish(response, flow, rewards, term);

                    Logutil.log("フロー番号_" + flow.getType());

                } else {

                    Logutil.log("API_エラー_フロー取得処理");

                    Log.e(TAG, response.errorMessage);
                    callback.onFinish(response, null, null, null);
                }
            }

            @Override
            public void onPre() { }
        }, true).execute(request);
    }


    // jsonからフロー情報を取得
    private static Flow getFlow(String strJson) {

        Flow flow = new Flow();

        flow.setCode(JsonUtil.getString(strJson, "flow_code"));
        flow.setType(JsonUtil.getInt(strJson, "flow_type"));
        flow.setRoboappMessage1(JsonUtil.getString(strJson, "roboapp_message1"));
        flow.setRoboappMessage2(JsonUtil.getString(strJson, "roboapp_message2"));
        flow.setRoboappMessage3(JsonUtil.getString(strJson, "roboapp_message3"));
        flow.setRoboappMessage4(JsonUtil.getString(strJson, "roboapp_message4"));

        return flow;
    }

    // jsonからリワードリストを取得
    private static ArrayList<Reward> getRewardList(String strJson) {

        ArrayList<Reward> rewards = new ArrayList<>();

        try {
            JSONObject rootJson = new JSONObject(strJson);
            JSONArray rewardsJson = rootJson.getJSONArray("reward_list"); // リワードリスト
            for (int i = 0; i < rewardsJson.length(); i++) { // リワードを取得
                Reward reward = new Reward();
                JSONObject rewardJson = rewardsJson.getJSONObject(i);
                reward.setType(JsonUtil.getInt(rewardJson.toString(), "type"));
                reward.setId(JsonUtil.getInt(rewardJson.toString(), "id"));
                reward.setTitle(JsonUtil.getString(rewardJson.toString(), "title"));
                reward.setLimitOver(JsonUtil.getBoolean(rewardJson.toString(), "limitover"));
                reward.setTermOver(JsonUtil.getBoolean(rewardJson.toString(), "termover"));
                rewards.add(reward);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return rewards;
    }

    // jsonから規約情報を取得
    private static Term getTerm(String strJson) {

        Term term = new Term();

        term.setTermSbr(JsonUtil.getString(strJson, "terms_sbr"));
        term.setTermUser(JsonUtil.getString(strJson, "terms_user"));

        return term;

    }

    public interface GetFlowCallback {
        void onFinish(HttpTask.HttpResponse response, Flow flow, ArrayList<Reward> rewards, Term term);
    }
}
