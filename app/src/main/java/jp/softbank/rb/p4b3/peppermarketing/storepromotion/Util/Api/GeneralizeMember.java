package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Logutil;

public class GeneralizeMember {

    private final String TAG = this.getClass().getSimpleName();

    private String accessToken;
    private int memberId;
    private GeneralizeMemberCallBack callback;

    // 本会員移行
    public GeneralizeMember(String accessToken, int memberId, GeneralizeMemberCallBack callback) {
        this.accessToken = accessToken;
        this.memberId = memberId;
        this.callback = callback;
    }

    public void excute() {

        Logutil.log("API_開始_本会員移行処理");

        // 本会員登録移行
        final String url = "https://rap-vm.softbankrobotics.com/portal/pmkt/api3.php/member/generalize";

        // POSTのヘッダー情報付加
        ArrayList<HttpTask.Param> headers = new ArrayList<>();
        headers.add(new HttpTask.Param("Content-Type", "application/x-www-form-urlencoded"));

        // POSTパラメータ
        String param = "access_token=" + accessToken + "&" +
                "member_id=" + String.valueOf(memberId);

        // 引数(URL,パラメータ,ヘッダー)
        final HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                param,
                headers
        );

        // 通信前処理、通信後処理を設定
        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    Logutil.log("API_終了_本会員移行処理");

                    // 通信成功
                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    // パース
                    boolean isSuccess = isSuccess(resultJson);
                    callback.onFinish(response, isSuccess);

                } else {

                    Logutil.log("API_エラー_本会員移行処理");

                    Log.e(TAG, response.errorMessage);
                    callback.onFinish(response, false);
                }
            }

            @Override
            public void onPre() { }
        }, true).execute(request);
    }

    // jsonから成功したかどうかを取得
    private boolean isSuccess(String strJson) {

        boolean success = false;

        try {
            JSONObject rootJson = new JSONObject(strJson);
            // keyで情報を取得
            success = rootJson.getBoolean("success");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return success;
    }

    public interface GeneralizeMemberCallBack {
        void onFinish(HttpTask.HttpResponse response, boolean isSuccess);
    }
}
