package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper;

public interface FutureInterface {
    void onSuccess();
    void onError(String error);
}
