package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model.Reward;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.R;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.View.PepperButton;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PepperMemory;

public class RewardListOneFragment extends MarketingFragment {

    // リワード
    Reward reward;

    PepperButton btnRewardOne;

    TextView txtRewardOne;

    TextView txtMesaage;

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        // 使用するレイアウト
        return inflater.inflate(R.layout.fragment_reward_list_one, container, false);
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        reward = PepperMemory.selectedReward;

        // UI 設定
        txtRewardOne = view.findViewById(R.id.txt_reward_one);
        txtRewardOne.setText(reward.getTitle());

        btnRewardOne = view.findViewById(R.id.btn_reward_one);
        btnRewardOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonClick();
            }
        });

        txtMesaage = view.findViewById(R.id.txt_reward_list_one_title);
        txtMesaage.setText(PepperMemory.flow.getRoboappMessage2());

        // 終了ボタン処理
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);
                buttonFinishListener.onClick(view);
            }
        });

        // 発話
        sayPepper();
    }

    // 発話
    private void sayPepper() {

        List<String> texts = Arrays.asList(
                getString(R.string.reward_list_one_a_1),
                getString(R.string.reward_list_one_a_2),
                getString(R.string.reward_list_one_a_3)
        );

        String speechText = texts.get((new Random()).nextInt(texts.size())) + PepperMemory.flow.getRoboappMessage1();

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, speechText, true, new ActionCallback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }

    private void buttonClick() {

        // ボタンを使用不可に
        allButtonSetActive(layoutRoot, false);

        activity.soundPool.play(activity.seDecide, 1.0f, 1.0f, 0, 0, 1.0f);

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, getString(R.string.reward_list_one_c), true, new ActionCallback() {
            @Override
            public void onSuccess() {

                switch (PepperMemory.flow.getType()) {
                    case 2201:
                        activity.changeFragment(new ReadQRFragment());
                        break;
                    case 2001:
                        activity.changeFragment(new EnterPhoneNumberFragment());
                        break;
                }
            }

            @Override
            public void onError(String errorMessage) {
                onSuccess();
            }

            @Override
            public void onCancel() {
                onSuccess();
            }
        });

        say.execute();
    }
}
