package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Logutil;


public class SearchMember {

    private final String TAG = this.getClass().getSimpleName();

    private String accessToken;
    private String searchType;
    private String number;
    private SearchMemberCallback callback;

    public SearchMember(String accessToken, String searchType, String number, SearchMemberCallback callback) {
        this.accessToken = accessToken;
        this.searchType = searchType;
        this.number = number;
        this.callback = callback;
    }

    // 会員検索
    public void execute() {

        Logutil.log("API_開始_会員検索処理");

        // 会員検索をする
        final String url = "https://rap-vm.softbankrobotics.com/portal/pmkt/api3.php/member/search";

        // POSTのヘッダー情報付加
        ArrayList<HttpTask.Param> headers = new ArrayList<>();
        headers.add(new HttpTask.Param("Content-Type", "application/x-www-form-urlencoded"));

        // POSTパラメータ
        String param = "access_token=" + accessToken + "&" +
                searchType + "=" + number;

        // 引数(URL,パラメータ,ヘッダー)
        final HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                param,
                headers
        );

        // 通信前処理、通信後処理を設定
        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    Logutil.log("API_終了_会員検索処理");

                    // 通信成功 = 会員情報あり
                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    int memberId = getMemberId(resultJson);
                    String memberUrl = getMemberUrl(resultJson);

                    callback.onFinish(response, memberId, memberUrl);

                } else {

                    Logutil.log("API_エラー_会員検索処理");

                    Log.d(TAG, "error: " + response.errorMessage + " " + searchType + "(" + number + ")");
                    callback.onFinish(response, -1, null);
                }
            }

            @Override
            public void onPre() { }
        }, true).execute(request);
    }

    // jsonからメンバーIDを取得
    private int getMemberId(String strJson) {

        int memberId = -1;

        try {
            JSONObject rootJson = new JSONObject(strJson);
            memberId = rootJson.getInt("member_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return memberId;
    }

    // jsonからメンバーIDを取得
    private String getMemberUrl(String strJson) {

        String memberUrl = "";

        try {
            JSONObject rootJson = new JSONObject(strJson);
            memberUrl = rootJson.getString("membersite_shorturl");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return memberUrl;
    }

    public static class SearchType {
        public static final String MEMBER_ID = "member_id";
        public static final String TEL = "tel";
        public static final String ORIGINAL_MEMBERSHIP_BARCODE = "original_membership_barcode";
    }

    public interface SearchMemberCallback {
        void onFinish(HttpTask.HttpResponse response, int memberId, String memberUrl);
    }
}
