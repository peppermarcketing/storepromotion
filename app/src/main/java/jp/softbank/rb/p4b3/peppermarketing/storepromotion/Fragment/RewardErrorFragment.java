package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.R;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic.Say;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.PepperUtil;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PepperMemory;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.View.AutoResizeTextView;

public class RewardErrorFragment extends MarketingFragment {

    View layoutBack;

    boolean isReselect = false;

    String failerReason = "";
    final String EXPIRED = "expired";         // 期限切れ
    final String DEPLETED = "depleted";       // 売り切れ
    final String DISTRIBUTED = "distributed"; // 重複

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // エラー理由取得
        Bundle bundle = getArguments();
        if (bundle != null) {
            failerReason = bundle.getString("failer", "");
        }
    }

    // Fragmentで表示するViewを作成するメソッド
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        if (failerReason.equals(EXPIRED)) {
            // 使用するレイアウト
            return inflater.inflate(R.layout.fragment_reward_error_expired, container, false);
        } else {
            // 使用するレイアウト
            return inflater.inflate(R.layout.fragment_reward_error_depleted_distributed, container, false);
        }
    }

    // Viewが生成し終わった時に呼ばれるメソッド
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // UI設定
        layoutBack = view.findViewById(R.id.layout_root);

        AutoResizeTextView txtRewardError = view.findViewById(R.id.txt_reward_error);

        String speechMessage = "";

        // 6パターン
        switch (PepperMemory.flow.getType()) {
            case 2201:
            case 2301:
            case 2001:

                switch (failerReason) {
                    case EXPIRED:
                        txtRewardError.setText(getText(R.string.txt_expired));
                        speechMessage = getString(R.string.coupon_expired);
                        break;
                    case DEPLETED:
                        txtRewardError.setText(getText(R.string.txt_depleted_reward));
                        // 京浜の数を確認
                        if (PepperMemory.rewards.size() > 1) {
                            // 複数ある
                            isReselect = true;
                            speechMessage = getString(R.string.reward_re_select);
                        } else {
                            speechMessage = getString(R.string.coupon_depleted);
                        }
                        break;
                    case DISTRIBUTED:
                        txtRewardError.setText(getText(R.string.txt_distributed_reward));
                        speechMessage = getString(R.string.coupon_distributed);
                        break;
                }

                break;

            case 2203:
            case 2303:
            case 2003:

                switch (failerReason) {
                    case EXPIRED:
                        txtRewardError.setText(getText(R.string.txt_expired));
                        speechMessage = getString(R.string.massage_expired);
                        break;
                    case DEPLETED:
                        txtRewardError.setText(getText(R.string.txt_depleted_message));
                        speechMessage = getString(R.string.massage_depleted);
                        break;
                    case DISTRIBUTED:
                        txtRewardError.setText(getText(R.string.txt_destributed_message));
                        speechMessage = getString(R.string.massage_distributed);
                        break;
                }

                break;
        }

        // 発話中なら発話中断
        sayCancel();

        // 発話
        say = new Say(qiContext, speechMessage, true, new ActionCallback() {
            @Override
            public void onSuccess() {
                if (isReselect) {
                    activity.changeFragment(new RewardListFragment());
                } else {
                    PepperUtil.finishApplication(activity);
                }
            }

            @Override
            public void onError(String errorMessage) {
                this.onSuccess();
            }

            @Override
            public void onCancel() {
                this.onSuccess();
            }
        });

        say.execute();
    }
}
