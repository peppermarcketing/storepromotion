package jp.softbank.rb.p4b3.peppermarketing.storepromotion;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.media.SoundPool;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.aldebaran.qi.sdk.QiContext;
import com.aldebaran.qi.sdk.QiSDK;
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks;
import com.aldebaran.qi.sdk.object.touch.TouchSensor;
import com.aldebaran.qi.sdk.object.touch.TouchState;
import com.aldebaran.robotservice.ICloudService;
import com.aldebaran.robotservice.RobotServiceUtil;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment.EnterPhoneNumberFragment;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment.ErrorFragment;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment.ReadQRFragment;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment.RewardErrorFragment;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment.RewardListFragment;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Fragment.RewardListOneFragment;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model.Flow;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model.Reward;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Model.Term;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.GetAccessToken;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api.GetFlow;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PackageNames;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.ActionCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.Action.Basic.Autonomous;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.PepperUtil;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.UUID.RobotServiceHandler;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Pepper.UUID.RobotUUIDCallback;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.PepperMemory;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Timeout;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class StorePromotionActivity extends AppCompatActivity implements RobotLifecycleCallbacks {

    public QiContext qiContext;
    boolean flagFragment = false;

    // SMS送信回数管理
    public int timeOfSend = 0;
    public final int limitOfSend = 4;

    Handler h = new Handler() ;
    final int BUMPER_TIME = 5;
    Timeout bumperTimeout = new Timeout(BUMPER_TIME);

    // P4B3との連携用
    final public int REQUEST_CODE = 1685;

    // タイムアウト
    public Timeout timeout;

    String TAG = StorePromotionActivity.class.getSimpleName();

    // UUID
    private ServiceConnection serviceConnection;
    RobotServiceHandler serviceHandler = new RobotServiceHandler(new RobotServiceHandler.Callback() {
        @Override
        public void onRobotUuidDRetrieved(String robotUUId) {

            // UUID取得時処理
            Log.d(TAG, "UUID: " + robotUUId);
            checkData();
        }
    });
    final RobotUUIDCallback uuidCallback = new RobotUUIDCallback(serviceHandler);

    // 効果音
    public SoundPool soundPool;
    public int seTap;
    public int seDecide;
    public int seCancel;
    public int seBarcode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_promotion);

        if (savedInstanceState == null) {
            flagFragment = true;
        }

        // バー非表示処理
        stickyImmersiveMode();

        // 効果音設定
        soundInit();

        // キーボードイベント検知
        setKeyboardEvent();

        // Register the RobotLifecycleCallbacks to this Activity.
        QiSDK.register(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // バー非表示処理
        stickyImmersiveMode();
    }

    @Override
    protected void onDestroy() {
        // Unregister all the RobotLifecycleCallbacks for this Activity.
        QiSDK.unregister(this);
        unbindService(serviceConnection);
        super.onDestroy();
    }

    @Override
    public void onRobotFocusGained(QiContext qiContext) {

        /*
         * 順番に処理
         *
         * オートノマス停止
         * バンパー設定
         * UUID取得
         * 初回起動（設定）確認
         * アクセストークン取得
         * フロー情報取得
         * アプリ開始
         * */

        this.qiContext = qiContext;

        if (flagFragment) {
            flagFragment = false;

            // オートノマス停止
            stopAutonomous();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE) {
            // 認証画面から来た
            if (resultCode == RESULT_OK) {

                // PIN認証成功、アカウント設定画面へいく
                String packageName = PackageNames.SETTING;
                String className = packageName + ".SettingActivity";
                Intent intent = new Intent();
                intent.putExtra("activate_mode", PackageNames.STOREPROMOTION);
                intent.setClassName(packageName,className);
                startActivity(intent);

            } else {
                // PIN認証失敗
                // P4B3設定画面へ遷移する
                sendBroadcast(new Intent("jp.softbank.rb.p4b3.p4b3settings.action.SETTING_MENU"));
            }
            
        } else {
            // お仕事からの起動？
            if (resultCode == RESULT_OK) {
                // TODO アプリ起動？
            }
        }
    }

    @Override
    public void onRobotFocusLost() {

        qiContext = null;
        Log.d(this.getClass().getSimpleName(), "onRobotFocusLost");

    }

    @Override
    public void onRobotFocusRefused(String reason) {

        Log.d(this.getClass().getSimpleName(), "onRobotFocusRefused");
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void stopAutonomous() {

        new Autonomous(qiContext, new ActionCallback() {
            @Override
            public void onSuccess() {
                bumperInit();
                getUuid();
            }

            @Override
            public void onError(String errorMessage) {
                onSuccess();
            }

            @Override
            public void onCancel() {
                onSuccess();
            }
        }).hold();
    }

    // バックバンパーセンサー起動
    private void bumperInit() {

        final StorePromotionActivity activity = this;

        bumperTimeout.setTimeoutListener(new Timeout.TimeoutListener() {
            @Override
            public void onTimeout() {

                if (timeout != null) {
                    timeout.stop();
                    bumperTimeout.setTimeoutListener(null);
                }

                // 初回起動、PIN画面へ行く
                // TODO 現状エラー
                String packageName = PackageNames.SETTING_P4B3;
                String className = packageName + ".ui.password.InputPasswordActivity";
                Intent intent = new Intent();
                intent.setClassName(packageName,className);
                startActivityForResult(intent, REQUEST_CODE);

//                // PIN認証成功、アカウント設定画面へいく
//                String packageName = PackageNames.SETTING;
//                String className = packageName + ".SettingActivity";
//                Intent intent = new Intent();
//                intent.putExtra("activate_mode", PackageNames.REGISTRATION);
//                intent.setClassName(packageName,className);
//                startActivity(intent);

            }
        });

        com.aldebaran.qi.sdk.object.touch.Touch touch = qiContext.getTouch();
        TouchSensor touchSensor = touch.getSensor("Bumper/Back");
        touchSensor.addOnStateChangedListener(new TouchSensor.OnStateChangedListener() {
            @Override
            public void onStateChanged(TouchState state) {
                if (state.getTouched()) {


                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            bumperTimeout.start();
                        }
                    });

                } else {

                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            bumperTimeout.resetCount();
                            bumperTimeout.stop();
                        }
                    });

                }
            }
        });
    }

    // UUID取得処理
    private void getUuid() {

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                ICloudService robotService = ICloudService.Stub.asInterface(service);
                try {
                    robotService.getRobotUUID(uuidCallback);
                } catch (RemoteException e) {
                    Log.e(TAG, "onServiceConnected: ", e);
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.w(TAG, "Robot service is disconnected");
            }
        };

        bindService(RobotServiceUtil.getCloudServiceIntent(), serviceConnection, BIND_AUTO_CREATE);
    }

    // 初回起動確認
    private void checkData() {

        try {
            Context context = createPackageContext(PackageNames.SETTING, CONTEXT_RESTRICTED);

            SharedPreferences sharedPreferences = context.getSharedPreferences(
                    PepperMemory.preferenceName,
                    Context.MODE_PRIVATE);

            String userName = sharedPreferences.getString("user_name", "");
            String password = sharedPreferences.getString("password", "");
            if (userName.equals("") || password.equals("")) {

                // 初回起動、PIN画面へ行く
                // TODO 現状エラー
                String packageName = PackageNames.SETTING_P4B3;
                String className = packageName + ".ui.password.InputPasswordActivity";
                Intent intent = new Intent();
                intent.setClassName(packageName,className);
                startActivityIfNeeded(intent, REQUEST_CODE);

            } else {
                // ペッパーメモリに追加
                PepperMemory.userId = userName;
                PepperMemory.password = password;

                getAccessToken();
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

            // APIエラー
            changeFragment(new ErrorFragment());
        }
    }

    // アクセストークン取得 (ロボットトークン使用)
    // TODO (UUIDでの取得になった後は削除予定)
    private void getAccessToken() {

        String userId = PepperMemory.userId;
        String password = PepperMemory.password;
        String robotToken = PepperMemory.robotToken;

        new GetAccessToken(robotToken, userId, password, new GetAccessToken.GetAccessTokenCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, String accessToken) {

                // 通信成功・失敗確認
                if (response.isSuccess) {

                    // アクセストークンをメモリに保存
                    PepperMemory.accessToken = accessToken;

                    // フロー情報取得
                    getFlow();
                } else {
                    // APIエラー
                    changeFragment(new ErrorFragment());
                }
            }
        }).execute();
    }

    // フロー情報取得
    private void getFlow() {

        String accessToken = PepperMemory.accessToken;
        String appId = PepperMemory.appIdPromo;

        new GetFlow(accessToken, appId, new GetFlow.GetFlowCallback() {
            @Override
            public void onFinish(HttpTask.HttpResponse response, Flow flow, ArrayList<Reward> rewards, Term term) {

                // 通信成功・失敗確認
                if (response.isSuccess) {

                    // TODO 以下テスト用コード
                    if (flow.getRoboappMessage1().length() == 0) {
                        flow.setRoboappMessage1("CMSから取得したフロー開始時のセリフです。");
                    }
                    if (flow.getRoboappMessage2().length() == 0) {
                        switch (flow.getType()) {
                            case 2001:
                                flow.setRoboappMessage2("今回のプレゼントはこちらです！");
                                break;
                            case 2003:
                                flow.setRoboappMessage2("お得なメッセージをお送りします！携帯電話の番号を入力してください");
                                break;
                            case 2201:
                                flow.setRoboappMessage2("今回のプレゼントはこちらです！");
                                break;
                            case 2203:
                                flow.setRoboappMessage2("お客様サイトに表示されたお客様番号のＱＲコードを\n赤枠の中にかざしてください");
                                break;
                            case 2301:
                                flow.setRoboappMessage2("配信したプレゼントのＱＲコードを\n赤枠の中にかざしてください");
                                break;
                            case 2303:
                                flow.setRoboappMessage2("配信したメッセージのＱＲコードを\n赤枠の中にかざしてください");
                                break;

                        }
                    }
                    if (flow.getRoboappMessage3().length() == 0) {
                        flow.setRoboappMessage3("CMSから取得したフロー終了時のセリフです。");
                    }
                    if (flow.getRoboappMessage4().length() == 0) {
                        switch (flow.getType()) {
                            case 2001:
                            case 2201:
                            case 2301:
                                flow.setRoboappMessage4("プレゼントを贈りました！！\n携帯に届いているか確認してね");
                                break;
                            case 2003:
                            case 2203:
                            case 2303:
                                flow.setRoboappMessage4("メッセージを贈りました！！\n携帯に届いているか確認してね");
                                break;
                        }
                    }

                    // フロー情報、リワードリストをメモリに保存
                    PepperMemory.flow = flow;
                    PepperMemory.rewards = checkRewards(rewards);
                    PepperMemory.term = term;

                    // アプリを起動
                    startApplication();
                } else {
                    // APIエラー
                    changeFragment(new ErrorFragment());
                }
            }
        }).excute();
    }

    // アプリ開始
    private void startApplication() {

        // 配信できるものがあるか調べる
        if (PepperMemory.rewards.size() == 0) {
            // 配信できるものが無い
            // 売り切れエラー
            switch (PepperMemory.flow.getType()) {
                case 2201:
                case 2203:
                case 2001:
                case 2003:
                    Bundle bundle = new Bundle();
                    bundle.putString("failer", "depleted");
                    Fragment fragment1 = new RewardErrorFragment();
                    fragment1.setArguments(bundle);
                    changeFragment(fragment1);
                    return;
            }
        }

                        /*
                        ・フローパターン
                        2201: Pepper会員証読込 + クーポン配信
                        2203: Pepper会員証読込＆メッセージ配信
                        2301: 配信QRコード(クーポン)読込
                        2303: 配信QRコード(メッセージ)読込
                        2001: アクション条件なし + クーポン配信
                        2003: アクション条件なし + メッセージ配信

                        ・分岐条件
                        クーポン配信
                            →配信QR以外
                                →リワードリスト画面（2201, 2001）
                            →配信QR
                                →QR読み込み画面（2301）
                         メッセージ配信（リワードは1件）
                            →アクション条件あり
                                →QR読み込み画面（2203, 2303）
                            →アクション条件なし
                                →電話番号入力画面（2003）

                        ・リワード
                        絶対1件
                          →メッセージ配信（配信QRの場合はまだ）（2203, 2003）
                        複数
                          →クーポン配信（配信QRの場合はまだ）（2201, 2001）
                        QR読み込みで1件取得できる
                          →配信QRの場合（2301, 2303）

                        */

        Fragment fragment = null;

        switch (PepperMemory.flow.getType()) {
            case 2201:
            case 2001:
                // リワードの個数によって画面が違う
                if (PepperMemory.rewards.size() == 1) {
                    // 1個だけ
                    PepperMemory.selectedReward = PepperMemory.rewards.get(0);
                    fragment = new RewardListOneFragment();
                } else {
                    fragment = new RewardListFragment();
                }
                break;

            case 2203:
            case 2301:
            case 2303:
                if (PepperMemory.flow.getType() == 2203) {
                    PepperMemory.selectedReward = PepperMemory.rewards.get(0);
                }
                fragment = new ReadQRFragment();
                break;

            case 2003:
                PepperMemory.selectedReward = PepperMemory.rewards.get(0);
                // 電話番号入力画面
                fragment = new EnterPhoneNumberFragment();
                break;
        }

        changeFragment(fragment);
    }

    // 有効なリワードを抽出
    private ArrayList<Reward> checkRewards(ArrayList<Reward> rewards) {
        ArrayList<Reward> effectiveRewars = new ArrayList<>();
        for (Reward reward : rewards) {
            if (!reward.isLimitOver() && !reward.isTermOver()) {
                effectiveRewars.add(reward);
            }
        }
        return effectiveRewars;
    }

    // 効果音
    private void soundInit() {
        soundPool = new SoundPool.Builder().setMaxStreams(3).build();
//        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
//            @Override
//            public void onLoadComplete(SoundPool soundPool, int i, int i1) {
//                if (i >= 4) { // 全部で4つ
//
//                }
//            }
//        });
        seTap = soundPool.load(this, R.raw.tap, 1);
        seDecide = soundPool.load(this, R.raw.decide, 2);
        seCancel = soundPool.load(this, R.raw.cancel, 3);
        seBarcode = soundPool.load(this, R.raw.barcode_get, 4);
    }

    // バー非表示処理
    private void stickyImmersiveMode(){
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        );

        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        // Note that system bars will only be "visible" if none of the
                        // LOW_PROFILE, HIDE_NAVIGATION, or FULLSCREEN flags are set.
                        if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                            Log.d("debug","The system bars are visible");
                        } else {
                            Log.d("debug","The system bars are NOT visible");
                        }
                    }
                });
    }

    // キーボード表示・非表示検知
    public final void setKeyboardEvent() {
        final View activityRootView = ((ViewGroup) findViewById(android.R.id.content)).getChildAt(0);

        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            private boolean wasOpened = false;

            private final Rect r = new Rect();

            @Override
            public void onGlobalLayout() {
                activityRootView.getWindowVisibleDisplayFrame(r);

                // 画面の高さとビューの高さを比べる
                int heightDiff = activityRootView.getRootView().getHeight() - r.height();

                boolean isOpen = heightDiff > 100;

                if (isOpen == wasOpened) {
                    // キーボードの表示状態は変わっていないはずなので何もしない
                    return;
                }

                wasOpened = isOpen;

                // 非商事になったら、バー非表示の処理をもう一回行う
                if (!isOpen) {
                    stickyImmersiveMode();
                }
            }
        });
    }

    public void changeFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .commit();
    }

    public void pinAuth() {

    }
}
