package jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Api;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.HttpTask;
import jp.softbank.rb.p4b3.peppermarketing.storepromotion.Util.Logutil;

public class TemporalRegister {

    private final String TAG = this.getClass().getSimpleName();

    private String accessToken;
    private String flowCode;
    private int flowType;
    private String pepperName;
    private String phoneNumber;
    private TemporalRegisterCallback callback;

    public TemporalRegister(String accessToken, String flowCode, int flowType, String pepperName, String phoneNumber, TemporalRegisterCallback callback) {
        this.accessToken = accessToken;
        this.flowCode = flowCode;
        this.flowType = flowType;
        this.pepperName = pepperName;
        this.phoneNumber = phoneNumber;
        this.callback = callback;
    }

    // 仮登録処理
    public void excute() {

        Logutil.log("API_開始_仮登録処理");


        final String url = "https://rap-vm.softbankrobotics.com/portal/pmkt/api3.php/member/temporal";

        // POSTのヘッダー情報付加
        ArrayList<HttpTask.Param> headers = new ArrayList<>();
        headers.add(new HttpTask.Param("Content-Type", "application/x-www-form-urlencoded"));

        // POSTパラメータ
        String param = "access_token=" + accessToken + "&" +
                "flow_type=" + String.valueOf(flowType) + "&" +
                "flow_code=" + flowCode + "&" +
                "pepper_name=" + pepperName + "&" +
                "tel=" + phoneNumber;

        // 引数(URL,パラメータ,ヘッダー)
        final HttpTask.HttpRequest request = new HttpTask.HttpRequest(
                url,
                param,
                headers
        );

        // 通信前処理、通信後処理を設定
        new HttpTask(new HttpTask.HttpListener() {
            @Override
            public void onFinish(HttpTask.HttpResponse response) {

                if (response.isSuccess) {

                    Logutil.log("API_終了_仮登録処理");

                    // 通信成功
                    String resultJson = HttpTask.byte2string(response.result);
                    Log.d(TAG, resultJson);

                    // パース
                    int memberId = getMemberId(resultJson);
                    String message = getMessage(resultJson);
                    callback.onFinish(response, memberId, message);

                } else {

                    Logutil.log("API_エラー_仮登録処理");

                    Log.e(TAG, response.errorMessage);
                    callback.onFinish(response, -1, null);
                }
            }

            @Override
            public void onPre() { }
        }, true).execute(request);
    }

    // jsonからメンバーIDを取得
    private int getMemberId(String strJson) {

        int memberId = -1;

        try {
            JSONObject rootJson = new JSONObject(strJson);
            memberId = rootJson.getInt("member_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return memberId;
    }

    // jsonからURLなどのメッセージを取得
    private String getMessage(String strJson) {

        String message = "";

        try {
            JSONObject rootJson = new JSONObject(strJson);
            if (!rootJson.isNull("pincode")) {
                message = rootJson.getString("pincode");
            } else if (!rootJson.isNull("registration_shorturl")) {
                message = rootJson.getString("registration_shorturl");
            } else if (!rootJson.isNull("blank_emailaddress")) {
                message = rootJson.getString("blank_emailaddress");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return message;
    }

    public interface TemporalRegisterCallback {
        void onFinish(HttpTask.HttpResponse response, int memberId, String message);
    }
}
